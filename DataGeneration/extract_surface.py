from copy import deepcopy
import random
import traceback
from cv2 import line, trace
import numpy as np
from numpy.core.numeric import full
from vtk import *
import os
import math
import sys
from vtkutils import writeMesh

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),"../Utils"))
from generalutils import *
from vtkutils import *

# sys.path.append("../")
# from BoundarySetup import setup_ligament

def randomSurface( fullSurface, wDistance=1, wNormal=1, wNoise=1, surfaceAmount=None, surfacePointMax=0, centerPointID=None, surface_with_displ_err=None ):
    """
    Extract a random part of a surface mesh. Starting from a (random) center node c, the
    surrounding points p are assigned three values:
        - Geodesic distance from the center node c
        - Angle difference between the normal of p and the normal of c
        - Random perlin noise value, sampled at the positon p.
    From these three values, we build a weighted sum, which acts as the likelyhood of a point
    being removed. We then select a threshold and remove all points whose likelyhood exceeds
    this threshold. The remaining points are the selected surface.
    The weights in the weighted sum can be used to influence whether the geodesic distance,
    the normals or the random perlin noise value should have a higher influence on the
    removal.

    Arguments:
    ----------
    fullSurface (vtkPolyData):
        Full surface. A part of this mesh will be returned. Point IDs and Cell IDs will not
        be kept.
    wDistance (float):
        Influence of the geodesic distance of a point to the center point c.
        If this value is > 0 and the other weights are == 0, only the distance will be
        taken into account.
    wNormal (float):
        Influece of the angle between normals.
        If this value is > 0 and the other weights are == 0, only points with a similar
        normal as the center point will be selected.only points with a similar
        normal as the center point will be selected.
    wNoise (float):
        Influence of the highest noise.
        If this value is > 0 and the other weights are == 0, entirely random parts of the
        surface will be selected.
    surfaceAmount (float):
        Amount of the surface which we want to select. If None, a random amount
        between 0 and 1 will be selected.
        Valid range: (0,1)
    centerPointID (int):
        Index of the center point c. If None, a random index of all the surface indices will be selected.

    Returns:
    ----------
    vtkPolyData
        Describes the part of the fullSurface which was selected
    """

    # Decide how many points we want to select:
    if surfaceAmount is None:
        surfaceAmount = random.random()
    assert surfaceAmount <= 1 and surfaceAmount > 0, "surfaceAmount must be between 0 and 1."

    # if hints_flag:
    #     if surfaceAmount < 0.12:
    #         surfaceAmount = 0.12
    # print("surface amount:", surfaceAmount)
    pointsToSelect = surfaceAmount*fullSurface.GetNumberOfPoints()
    if surfacePointMax > 0 and surfacePointMax < pointsToSelect:
        pointsToSelect = surfacePointMax
    print("point to select:", pointsToSelect)

    # if hints_flag:
    #     if pointsToSelect < 30:
    #         print("pointsToSelect too small, choose a greater one, now is:", pointsToSelect)
            # pointsToSelect = random.randint()


    
    fullSurface = generatePointNormals( fullSurface )
    normals = fullSurface.GetPointData().GetArray( "Normals" )

    # Store the IDs of all triangles:
    #triangleIDs = []
    #for i in range(0,fullSurface.GetNumberOfCells()):
    #    if fullSurface.GetCell(i).GetCellType() == VTK_TRIANGLE:
    #        triangleIDs.append(i)

    # Select a random point on the surface around which to center the selected part, if it is not provided:
    if centerPointID is None:
        centerPointID = random.randint(0,fullSurface.GetNumberOfPoints()-1)
        for idx_r in range(0, 5):
            print(random.randint(0,fullSurface.GetNumberOfPoints()-1))
        print("center point id:", centerPointID)
    centerPoint = fullSurface.GetPoint( centerPointID )

    distance = geodesicDistance( fullSurface, centerPointID )
    fullSurface.GetPointData().AddArray( distance )


    # Get normal of that point:
    centerPointNormal = normals.GetTuple3( centerPointID )


    # Decrease with:
    # - distance from center point
    # - normal difference
    # - perlin noise

    noise = vtkPerlinNoise()
    noise.SetFrequency( 15, 15, 15 )
    noise.SetPhase( random.random()*150, random.random()*150, random.random()*150 )

    # Create an array which will be filled and then used for thresholding
    likelyhood = vtkDoubleArray()
    likelyhood.SetNumberOfComponents(1)
    likelyhood.SetNumberOfTuples(fullSurface.GetNumberOfPoints())
    likelyhood.SetName("likelyhood")

    if surface_with_displ_err:
        if not fullSurface.GetNumberOfPoints() == surface_with_displ_err.GetNumberOfPoints():
            print("not equal")
            return None
        displ_err_array = surface_with_displ_err.GetPointData().GetArray("displacementError")
        displacement_error_array = [np.linalg.norm(displ_err_array.GetTuple(idx)) for idx in range(fullSurface.GetNumberOfPoints())]
        displ_err_max = max(displacement_error_array)
        displ_err_min = min(displacement_error_array)
        displacement_error_array_normalized = [(x - displ_err_min)/(displ_err_max - displ_err_min) for x in displacement_error_array]
        # displacement_error_array = []
        # print(fullSurface.GetNumberOfPoints())
        # for idx in range(fullSurface.GetNumberOfPoints()):
        #     displacement_error_array.append(weightDisplErrArray.GetTuple1(idx))
        #     print(111)
        # displ_err_max = max(displacement_error_array)
        # displ_err_min = min(displacement_error_array)
        # displacement_error_array_normalized = [(x - displ_err_min)/(displ_err_max - displ_err_min) for x in displacement_error_array]
        
    minVal = 99999
    maxVal = -1
    for i in range( fullSurface.GetNumberOfPoints() ):
        pt = fullSurface.GetPoint( i )
        dist = math.sqrt( vtkMath.Distance2BetweenPoints(centerPoint, pt) )

        normal = normals.GetTuple3( i )
        dot = vtkMath.Dot( centerPointNormal, normal )
        normalAng = math.acos( max( min( dot, 1 ), -1 ) )

        rnd = abs(noise.EvaluateFunction( pt ))

        if surface_with_displ_err:
            wDisplErr = 3 # 1
            eps = 1
            displErr = 1 / (displacement_error_array_normalized[i] + eps)
        else:
            wDisplErr = 0
            eps = 0
            displErr = 0

        curLikelyhood = wDistance*dist + wNormal*normalAng + wNoise*rnd + wDisplErr*displErr
        # print(curLikelyhood, wDisplErr*displErr)
        # print("weight of displacement error:", wDisplErr*displErr)
        # print("likelyhood items:", "dist:", wDistance*dist, "normal:", wNormal*normalAng, "noise:", wNoise*rnd)
        likelyhood.SetTuple1( i, curLikelyhood )
        minVal = min( minVal, curLikelyhood )
        maxVal = max( maxVal, curLikelyhood )

    #print("Likelyhood range:", minVal, maxVal)

    # Build histogramm of likelyhoods:
    # histBins = 50
    histBins = 100
    hist = [0]*histBins
    for i in range( fullSurface.GetNumberOfPoints() ):
        l = likelyhood.GetTuple1( i )
        curBin = int(l/maxVal*(histBins-1))
        hist[curBin] += 1
   
    # Find out where to set the threshold so that surfaceAmount points are selected.
    # We do this by going through the histogram and summing up the values in the bins. As
    # soon as more than surfaceAmount points are selected, 
    thresholdedPoints = 0
    threshold = maxVal  # Start with default of selecting everything
    for i in range( histBins ):
        thresholdedPoints += hist[i]
        if thresholdedPoints >= pointsToSelect:
            threshold = (i+1)/histBins*maxVal
            break
    #print("Selected threshold", threshold)

    fullSurface.GetPointData().AddArray( likelyhood )

    likelyhoodRange = maxVal - minVal

    #rndThreshold = (random.random()*0.75 + 0.25)*likelyhoodRange + minVal


    thresh = vtkThreshold()
    thresh.SetInputData( fullSurface )
    thresh.SetInputArrayToProcess( 0,0,0,
            vtkDataObject.FIELD_ASSOCIATION_POINTS, "likelyhood" )
    thresh.ThresholdBetween( 0, threshold )
    thresh.Update()

    # Write resulting surface to file:
    # Debug output:
    # writer = vtkXMLPolyDataWriter()
    # writer.SetFileName( "partialSurfaceLikelyhood.vtp" )
    # writer.SetInputData( fullSurface )
    # writer.Update()

    partialSurface = unstructuredGridToPolyData( thresh.GetOutput() )


    # if partialSurface.GetNumberOfPoints() > pointsToSelect * 1.5 :
    #     # threshold_new = (pointsToSelect / partialSurface.GetNumberOfPoints()) * threshold
    #     threshold_new = threshold * 0.9
    #     print("partial surface point number: {}, new threshold: {}, (old: {})".format(partialSurface.GetNumberOfPoints(), threshold_new, threshold))
    #     thresh = vtkThreshold()
    #     thresh.SetInputData( fullSurface )
    #     thresh.SetInputArrayToProcess( 0,0,0,
    #             vtkDataObject.FIELD_ASSOCIATION_POINTS, "likelyhood" )
    #     thresh.ThresholdBetween( 0, threshold_new )
    #     thresh.Update()
    #     partialSurface = unstructuredGridToPolyData( thresh.GetOutput() )
    #     print("new partial surface point number: {}".format(partialSurface.GetNumberOfPoints()))

    fullArea = surfaceArea( fullSurface )
    partialArea = surfaceArea( partialSurface )

    #print( "Original area:", fullArea )
    #print( "Partial area:", partialArea )
    #print( "Selected amount: {:.2f}% (Target was {:.2f}%)".format( 100*partialArea/fullArea,
    #    100*surfaceAmount ) )

    return partialSurface



def extract_surface_round(surface, radius=None, center_point=None, surface_amount=0 ):
    try:
        print("extracting round sub-surface..........")

        if not center_point:
            center_point = random.randint(0, surface.GetNumberOfPoints() - 1)
        print("center point id:", center_point)

        # cannot use geodesic distance since intraoperative surface is pieced
        # distance = geodesicDistance( surface, center_point )
        # surface.GetPointData().AddArray( distance )
        surface = euclideanDistance( surface, center_point)

        surface = generatePointNormals( surface )
        normals = surface.GetPointData().GetArray( "Normals" )

        # criterion = vtkDoubleArray()
        # criterion.SetNumberOfComponents(1)
        # criterion.SetNumberOfTuples(surface.GetNumberOfPoints())
        # criterion.SetName("criterion")

        thresh = vtkThreshold()
        thresh.SetInputData( surface )
        thresh.SetInputArrayToProcess( 0,0,0, vtkDataObject.FIELD_ASSOCIATION_POINTS, "euclidean_distance" )

        if not surface_amount:
            if not radius:
                radius = random.gauss(0.06, 0.01)
            print("radius:", radius)
            distance_threshod = radius
        else:
            print("surface amount:", surface_amount)
            distance = surface.GetPointData().GetArray("euclidean_distance")
            distance_list = [distance.GetTuple1(idx) for idx in range(0, surface.GetNumberOfPoints())]
            distance_list.sort()
            # print(distance_list)
            distance_threshod = distance_list[math.floor(surface_amount * surface.GetNumberOfPoints())]
            print("list length: {}, threshold index {}, distance threshold:{}".format(len(distance_list), math.floor(surface_amount * surface.GetNumberOfPoints()), distance_threshod))
        thresh.ThresholdBetween( 0, distance_threshod )
        thresh.Update()
        partial_surface = unstructuredGridToPolyData( thresh.GetOutput() )
        print("extracted sub surface, point number:", partial_surface.GetNumberOfPoints())

        return partial_surface

    except Exception as e:
        traceback.print_exc()



# def extract_surface_line(surface, start_point=None, line_length=None, thickness=0):
#     try:
#         print("extracting surfaces in line shape")

#         if not start_point:
#             start_point = random.randint(0, surface.GetNumberOfPoints())
#         print("start point id:", start_point)
#         if not line_length:
#             line_length = random.gauss(0.06, 0.02)
#         print("path length:", line_length)
#         point_path = setup_ligament.selectSurfacePath(surface=surface, startNodeID=start_point, pathLength=line_length)
#         print(point_path)

#         point_coord_list = [surface.GetPoint(point_id) for point_id in point_path]
#         print(point_coord_list)


#         points_start = deepcopy(point_path)
#         points_next_layer = []
#         if thickness > 0 :
#             while True:
#                 print(points_start)
#                 for idx_p in points_start:
#                     points_connected = getConnectedVertices(surface, idx_p)
#                     # print(points_connected)
#                     for idx_a in points_connected:
#                         # dist_to_
#                         if not idx_a in points_next_layer and not idx_a in point_path and np.linalg.norm(np.asarray(surface.GetPoint(idx_p)) - np.asarray(surface.GetPoint(idx_a))) <= thickness:
#                             points_next_layer.append(idx_a)
#                             point_path.append(idx_a)
#                     points_start = points_next_layer
#                     points_next_layer = []
#                 if not points_start:
#                     break

#         point_coord_list = [surface.GetPoint(point_id) for point_id in point_path]
#         print(point_coord_list)

#         points_poly = convert_point_coords_2_poly_1(point_coord_list)

#         return points_poly
#     except Exception as e:
#         traceback.print_exc()



# def extract_surface_line_brush(surface, start_point=None, line_length=None, brush=0):
#     try:
#         print("extracting surfaces in line shape")

#         if not start_point:
#             start_point = random.randint(0, surface.GetNumberOfPoints())
#         print("start point id:", start_point)
#         if not line_length:
#             line_length = random.gauss(0.06, 0.02)
#         print("path length:", line_length)
#         point_path = setup_ligament.selectSurfacePath(surface=surface, startNodeID=start_point, pathLength=line_length, brush=brush)
#         print(point_path)


#         point_coord_list = [surface.GetPoint(point_id) for point_id in point_path]
#         print(point_coord_list)

#         points_poly = convert_point_coords_2_poly_1(point_coord_list)

#         return points_poly
#     except Exception as e:
#         traceback.print_exc()
