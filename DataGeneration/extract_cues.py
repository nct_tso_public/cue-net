import argparse, sys
from functools import partial
import traceback
import logging
import vtk
import os
import traceback
# sys.path.append("./")
import extract_surface
import generalutils as utils
import random
import vtkutils
import voxelize
import numpy as np
import yaml


def combine_partial_surfaces(surface_list, deformed_surface, init_surface, cur_dir):
    """
        Combine multiple surfaces into one, used for visualization
    """
    overlap_matrix = [[0] * len(surface_list) for i in range(deformed_surface.GetNumberOfPoints())]

    append_filter = vtk.vtkAppendFilter()
    for s in range(0, len(surface_list)):
        indexarray = surface_list[s].GetPointData().GetArray("pointindex")
        append_filter.AddInputData(surface_list[s])
        append_filter.Update()
        for i in range(0, surface_list[s].GetNumberOfPoints()):
            overlap_matrix[int(indexarray.GetTuple1(i))][s] = 1

    combined_grid = append_filter.GetOutput()

    hints_index = deformed_surface.GetPointData().GetArray("hints_index")
    hints_index_array = deformed_surface.GetPointData().GetArray("hints_index_array")
    # overlap_matrix[points_total+1][1] = 0
    hints_index_init = init_surface.GetPointData().GetArray("hints_index")
    hints_index_array_init = init_surface.GetPointData().GetArray("hints_index_array")
    counter = 0
    hints_points_counter = 0
    for l in overlap_matrix:
        weight = 0
        value = 0
        for v in l:
            if v > 0:
                value = value + (v << weight)
            weight = weight + 1
        hints_index.SetTuple1(counter, value)
        hints_index_init.SetTuple1(counter, value)
        hints_index_array.SetTuple(counter, l + [0] * (8 - len(l)))
        hints_index_array_init.SetTuple(counter, l + [0] * (8 - len(l)))
        #print(hints_index_array.GetTuple(counter))
        #print(l)
        # print(l, value, counter)
        if value > 0:
            hints_points_counter = hints_points_counter + 1
        counter = counter + 1

    writer = vtk.vtkXMLPolyDataWriter()
    writer.SetFileName(os.path.join(cur_dir, "deformedSurface_hints.vtp"))
    writer.SetInputData(deformed_surface)
    writer.Update()

    writer = vtk.vtkXMLPolyDataWriter()
    writer.SetFileName(os.path.join(cur_dir, "initSurface_hints.vtp"))
    writer.SetInputData(init_surface)
    writer.Update()

    writer = vtk.vtkXMLUnstructuredGridWriter()
    writer.SetFileName(os.path.join(cur_dir, "hints_combined.vtu"))
    writer.SetInputData(combined_grid)
    writer.Update()
    print("hints points counter:", hints_points_counter)
    return combined_grid



def write_log(cur_dir, center_point_list, partial_surface_list, total_point, total_point_combined, total_point_surface):
    """
        Write out log (not in use)
    """
    try:
        summary = {
            "center_point_list": center_point_list,
            "partial_surface_list": partial_surface_list,
            "total_point": total_point,
            "total_point_combined": total_point_combined,
            "total_point_surface": total_point_surface,
        }
        with open(os.path.join(cur_dir, "hints_summary.yml"), 'w') as f:
            yaml.dump(summary, f, default_flow_style=False)
        return True
    except Exception as e:
        print(e)
        traceback.print_tb(e)
        return False



def map_displacement_error_to_intra_surface(cur_dir):
    """
        Map displacement error from preoperative surface to intraoperative surface
    """
    try:
        pre_surface_withDisplError_wrong_pointindex = vtkutils.loadMesh(os.path.join(cur_dir, "initSurface_hints_withDispl.vtu"))
        intra_partial_surface = vtkutils.loadMesh(os.path.join(cur_dir, "partialDeformedSurface.vtp"))
        voxel_grid = vtkutils.loadStructuredGrid(os.path.join(cur_dir, "voxelized.vts"))
        if os.path.exists(os.path.join(cur_dir, "deformedSurface_hints.vtp")):
            deformed_surface = vtkutils.loadMesh(os.path.join(cur_dir, "deformedSurface_hints.vtp"))
        else:
            original_base = "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/data/V2SDataStaticSolverNoRigid"
            origin_dir = os.path.join(original_base, os.path.basename(cur_dir))
            deformed_surface = vtkutils.loadMesh(os.path.join(origin_dir, "deformedSurface_hints.vtp"))
    
        # get displacement error for intra surface from init surface
        displacement_error_intra = vtk.vtkFloatArray()
        displacement_error_intra.SetNumberOfComponents(3)  # length of a tuple
        displacement_error_intra.SetNumberOfTuples(intra_partial_surface.GetNumberOfPoints())  # length of this array (how many tuples)
        displacement_error_intra.SetName("displacementError")

        point_index_from_distance = vtk.vtkFloatArray()
        point_index_from_distance.SetNumberOfComponents(1)  # length of a tuple
        point_index_from_distance.SetNumberOfTuples(intra_partial_surface.GetNumberOfPoints())  # length of this array (how many tuples)
        point_index_from_distance.SetName("point_index_distance")

        point_index = intra_partial_surface.GetPointData().GetArray("pointindex")
        displacement_error_pre = pre_surface_withDisplError_wrong_pointindex.GetPointData().GetArray("displacementError")

        pointLocator = vtk.vtkPointLocator()
        pointLocator.SetDataSet(deformed_surface)
        pointLocator.SetNumberOfPointsPerBucket(1)
        pointLocator.BuildLocator()
        weights = []
        for idx in range(intra_partial_surface.GetNumberOfPoints()):
            # p_idx_on_pre = int(point_index.GetTuple1(idx))
            point_intra_surface = intra_partial_surface.GetPoint(idx)
            point_idx_deformed_surface = pointLocator.FindClosestPoint(point_intra_surface)
            #  point indices from deformed surface and initial surface are the same
            point_index_from_distance.SetTuple1(idx, point_idx_deformed_surface)
            displacement_error_intra.SetTuple(idx, displacement_error_pre.GetTuple(point_idx_deformed_surface))
            displ_err_mag = np.linalg.norm( np.array(displacement_error_pre.GetTuple(point_idx_deformed_surface)) )
            # print(displ_err_mag)
            weights.append(displ_err_mag * 100)

        intra_partial_surface.GetPointData().AddArray(displacement_error_intra)
        intra_partial_surface.GetPointData().AddArray(point_index_from_distance)

        # print("write partial surface with displacement error...")
        vtkutils.writeMesh(intra_partial_surface, os.path.join(cur_dir, "partialDeformedSurface_withDisplError.vtp"))
        return intra_partial_surface
    except Exception as e:
        traceback.print_exc()
        return None


def select_center_points_weighted_intra(cur_dir, intra_partial_surface=None, hints_number_total=4):
    try:
        if not intra_partial_surface:
            intra_partial_surface = map_displacement_error_to_intra_surface(cur_dir=cur_dir)
        else:
            intra_partial_surface = intra_partial_surface
        displace_error_intra = intra_partial_surface.GetPointData().GetArray("displacementError")
        weights = []
        for idx in range(intra_partial_surface.GetNumberOfPoints()):
            displ_err_mag = np.linalg.norm( np.array(displace_error_intra.GetTuple(idx)) )
            weights.append(displ_err_mag * 100)

        weights_normalized = [ (x - min(weights)) / (max(weights) - min(weights)) for x in weights]

        center_point_id_list = random.choices(
            population = np.arange(intra_partial_surface.GetNumberOfPoints()),
            weights=weights_normalized,
            k=hints_number_total,
        )
        print("center points:", center_point_id_list, "weights:", [weights_normalized[c] for c in center_point_id_list], "max weight in list:", max(weights_normalized), "min weight in list", min(weights_normalized))
        return center_point_id_list
    except Exception as e:
        traceback.print_exc()




def extract_partial_surfaces_intraop(cur_dir=None, deformedSurface=None, weighted_cp=False, hints_shape="random"):
    """
        Extract four partial surfaces on the intraperative surface as intraoperative cues
    """
    try:

        hints_num_max = 4

        if not cur_dir:
            raise ValueError( "Path is none ")
        if not os.path.exists(cur_dir):
            raise ValueError( "Path {} not exists ".format(cur_dir))

        partial_deformed_surface_path = os.path.join(cur_dir, "partialDeformedSurface.vtp")

        if deformedSurface:
            deformedSurface = deformedSurface
        else:            
            deformedSurface = vtkutils.loadMesh(partial_deformed_surface_path)

        # clean up
        # file_list = os.listdir(cur_dir)
        # for pds in list(filter(lambda x: str(x).startswith("partial_deformed_surface_intra"), file_list)):
        #     os.remove(os.path.join(cur_dir, pds))
        # # if "deformedSurface_hints.vtp" in file_list:
        # #     os.remove(os.path.join(cur_dir, "deformedSurface_hints.vtp"))
        # if "combined_grid.vtu" in file_list:
        #     os.remove(os.path.join(cur_dir, "combined_grid.vtu"))

        point_index_intra = vtk.vtkIntArray()
        point_index_intra.SetNumberOfComponents(1)  # length of a tuple
        point_index_intra.SetNumberOfTuples(deformedSurface.GetNumberOfPoints())  # length of this array (how many tuples)
        point_index_intra.SetName("pointindex_intra")
        if deformedSurface.GetPointData().HasArray("pointindex_intra"):
            deformedSurface.GetPointData().RemoveArray("pointindex_intra")
        deformedSurface.GetPointData().AddArray(point_index_intra)
        # print("total points: {num}".format(num=surface.GetNumberOfPoints()))
        for i in range(0, deformedSurface.GetNumberOfPoints()):
            point_index_intra.SetTuple1(i, i)

        hints_index_array = vtk.vtkIntArray()
        hints_index_array.SetNumberOfComponents(hints_num_max)  # length of a tuple
        hints_index_array.SetNumberOfTuples(deformedSurface.GetNumberOfPoints())  # length of this array (how many tuples)
        hints_index_array.SetName("hints_index_array")
        if deformedSurface.GetPointData().HasArray("hints_index_array"):
            deformedSurface.GetPointData().RemoveArray("hints_index_array")
        deformedSurface.GetPointData().AddArray(hints_index_array)
        for i in range(0, deformedSurface.GetNumberOfPoints()):
            hints_index_array.SetTuple(i, [-1] * hints_num_max)

        hints_number_total = 4

        intra_surface_with_displError=None
        if weighted_cp:
            intra_surface_with_displError = map_displacement_error_to_intra_surface(cur_dir=cur_dir)
            center_point_id_list = select_center_points_weighted_intra(cur_dir=cur_dir, intra_partial_surface=intra_surface_with_displError)
        else:
            center_point_id_list = []
            for i in range(0, hints_number_total):
                center_point_id_list.append(random.randint(0, deformedSurface.GetNumberOfPoints() - 1))
        # print(center_point_id_list)

        # areaDef = vtkutils.surfaceArea(deformedSurface)
        # if areaDef < min_surface_area:
        #     raise ValueError("Surface area is too small: {:.4f}, something went wrong.".format(areaDef))

        # maxDispl = vtkutils.getMaximumDisplacement(initial_model, deformed_model)
        # if maxDispl < min_displacement:
        #     raise ValueError(
        #         "Organ did not deform enough during simulation, maximum displacement is {:.4f}.".format(maxDispl))
        # elif maxDispl > max_displacement:
        #     raise ValueError(
        #         "Organ did deform too much during simulation, maximum displacement is {:.4f}.".format(maxDispl))

        # surface_amount_min = 0.03 # 0.08  # 0.05 #0.02
        # surface_amount_max = 0.05 # 0.25  # 0.5
        surface_amount_min = 0.08  # 0.05 #0.02
        surface_amount_max = 0.25  # 0.5
        weight_distance_min = 1  # how much we want to account for the distance from c
        weight_distance_max = 5  # 10
        weight_normal_min = 5  # high values here mean that points belonging to the same surface patch as c will be kept.
        weight_normal_max = 10
        weight_noise_min = 1  # how much we want noise to influence roi selection
        weight_noise_max = 3

        partial_deformed_surface_list = []
        for c in range(0, len(center_point_id_list)):
            # Select between 10% and 100% of the surface:
            rndSurfaceAmount = random.uniform(surface_amount_min, surface_amount_max)
            rndWeightDistance = random.uniform(weight_distance_min, weight_distance_max)
            rndWeightNormal = random.uniform(weight_normal_min, weight_normal_max)
            rndWeightNoise = random.uniform(weight_noise_min, weight_noise_max)
            print(rndSurfaceAmount, rndWeightDistance, rndWeightNormal, rndWeightNoise)
            partial_surface_valid = False
            # extract partial surface until a valid one
            while not partial_surface_valid:
                if hints_shape == "random":
                    partial_deformed_surface = extract_surface.randomSurface(deformedSurface,
                                                                            wDistance=rndWeightDistance,
                                                                            wNormal=rndWeightNormal,
                                                                            wNoise=rndWeightNoise,
                                                                            surfaceAmount=rndSurfaceAmount,
                                                                            surfacePointMax= 50,
                                                                            centerPointID=center_point_id_list[c],
                                                                            #  weightDisplErrArray=intra_surface_with_displError.GetPointData().GetArray("displacementError"),)
                                                                            #hints_flag=True)
                                                                            surface_with_displ_err=intra_surface_with_displError,)
                elif hints_shape == "round":
                    partial_deformed_surface = extract_surface.extract_surface_round(deformedSurface,
                                                                                    surface_amount=rndSurfaceAmount,

                                                                                    )
                if partial_deformed_surface.GetNumberOfPoints() < 3:
                    print("points number of partial surface less than three, generate a new one...")
                else:
                    partial_surface_valid = True
            partial_deformed_surface_list.append(partial_deformed_surface)
            # point_index_array = partial_deformed_surface.GetPointData().GetArray("pointindex")
            # point_index_array_list = []
            # for i in range(0, partial_deformed_surface.GetNumberOfPoints()):
            #     point_index_array_list.append(point_index_array.GetTuple1(i))
            print("partial surface number:{counter}, point index array (length: {length})of {center_point_id}".format(
                # length=len(point_index_array_list),
                counter=c,
                length=partial_deformed_surface.GetNumberOfPoints(),
                center_point_id=center_point_id_list[c],
                # point_index_array_list=point_index_array_list
                )
            )
            enable_debug = False
            if enable_debug:
                writer = vtk.vtkXMLPolyDataWriter()
                writer.SetFileName(os.path.join(cur_dir, "partial_deformed_surface_intra_%s.vtp" % c))
                writer.SetInputData(partial_deformed_surface)
                writer.Update()

        # combined_grid = combine_partial_surfaces(partial_deformed_surface_list, surface, preSurface, cur_dir)
        overlap_matrix = [[0] * len(partial_deformed_surface_list) for i in range(deformedSurface.GetNumberOfPoints())]
        print("point number of deformedSurface", deformedSurface.GetNumberOfPoints())
        hints_index_array = deformedSurface.GetPointData().GetArray("hints_index_array")
        # overlap_matrix[points_total+1][1] = 0
        # append_filter = vtk.vtkAppendFilter()
        for s in range(0, len(partial_deformed_surface_list)):
            indexarray = partial_deformed_surface_list[s].GetPointData().GetArray("pointindex_intra")
            # print(partial_deformed_surface_list[s].GetPointData().HasArray("pointindex_intra"))
            # append_filter.AddInputData(partial_deformed_surface_list[s])
            # append_filter.Update()
            for i in range(0, partial_deformed_surface_list[s].GetNumberOfPoints()):
                # print(int(indexarray.GetTuple1(i)))
                overlap_matrix[int(indexarray.GetTuple1(i))][s] = 1

        counter = 0
        hints_points_counter = 0
        for l in overlap_matrix:
            hints_index_array.SetTuple(counter, l + [0] * (hints_num_max - len(l)))
            counter = counter + 1

        save_debug_flag = True
        if save_debug_flag:
            # combined_grid = append_filter.GetOutput()
            # writer = vtk.vtkXMLUnstructuredGridWriter()
            # writer.SetFileName(os.path.join(cur_dir, "hints_combined.vtu"))
            # writer.SetInputData(combined_grid)
            # writer.Update()
            # print("{} saved".format(os.path.join(cur_dir, "hints_combined.vtu")))
            # vtkutils.writeMesh()
            for idx_h in range(0, len(partial_deformed_surface_list)):
                hints_index_array = vtk.vtkIntArray()
                hints_index_array.SetNumberOfComponents(1)  # length of a tuple
                hints_index_array.SetNumberOfTuples(deformedSurface.GetNumberOfPoints())  # length of this array (how many tuples)
                hints_index_array.SetName("hints_index_array_{}".format(idx_h))
                if deformedSurface.GetPointData().HasArray("hints_index_array_{}".format(idx_h)):
                    deformedSurface.GetPointData().RemoveArray("hints_index_array_{}".format(idx_h))
                deformedSurface.GetPointData().AddArray(hints_index_array)
                indexarray = partial_deformed_surface_list[idx_h].GetPointData().GetArray("pointindex_intra")
                for i in range(0, deformedSurface.GetNumberOfPoints()):
                    hints_index_array.SetTuple1(i, 0)
                for i in range(0, partial_deformed_surface_list[idx_h].GetNumberOfPoints()):
                    hints_index_array.SetTuple1(int(indexarray.GetTuple1(i)), 1)

        writer = vtk.vtkXMLPolyDataWriter()
        # writer.SetFileName(os.path.join(cur_dir, "partialDeformedSurface_hints.vtp"))
        writer.SetFileName(os.path.join(cur_dir, "partialDeformedSurface.vtp"))
        writer.SetInputData(deformedSurface)
        writer.Update()
        print("{} saved".format(os.path.join(cur_dir, "partialDeformedSurface.vtp")))

        del deformedSurface, partial_deformed_surface_list
        return True

    except Exception as e:
        return False



def mark_hints_pre(cur_dir, partialDeformedSurface=None, pre_surface=None, intra_surface=None, weighted_cp=False):
    """
        Obtain preoperative cues 
    """
    try:
        print("mark hints from intra surface to pre:----------")
        hints_num_max = 4

        if not partialDeformedSurface:
            partialDeformedSurface = vtkutils.loadMesh(os.path.join(cur_dir, "partialDeformedSurface.vtp"))
        if not pre_surface:
            pre_model = vtkutils.loadMesh(os.path.join(cur_dir, "volume.vtk"))
            pre_surface = vtkutils.extractSurface(pre_model)
        else:
            pre_surface = pre_surface
        if not intra_surface:
            intra_model = vtkutils.loadMesh(os.path.join(cur_dir, "deformed.vtu"))
            intra_surface = vtkutils.extractSurface(intra_model)
        else:
            intra_surface = intra_surface

        if not partialDeformedSurface.GetPointData().HasArray("hints_index_array"):
            raise ValueError("no hints index array")
        
        hints_index_array = vtk.vtkIntArray()
        hints_index_array.SetNumberOfComponents(hints_num_max)  # length of a tuple
        hints_index_array.SetNumberOfTuples(pre_surface.GetNumberOfPoints())  # length of this array (how many tuples)
        hints_index_array.SetName("hints_index_array")
        if pre_surface.GetPointData().HasArray("hints_index_array"):
            pre_surface.GetPointData().RemoveArray("hints_index_array")
        pre_surface.GetPointData().AddArray(hints_index_array)
        for i in range(0, pre_surface.GetNumberOfPoints()):
            hints_index_array.SetTuple(i, [0] * hints_num_max)

        hints_index_array = vtk.vtkIntArray()
        hints_index_array.SetNumberOfComponents(hints_num_max)  # length of a tuple
        hints_index_array.SetNumberOfTuples(intra_surface.GetNumberOfPoints())  # length of this array (how many tuples)
        hints_index_array.SetName("hints_index_array")
        if intra_surface.GetPointData().HasArray("hints_index_array"):
            intra_surface.GetPointData().RemoveArray("hints_index_array")
        intra_surface.GetPointData().AddArray(hints_index_array)
        for i in range(0, intra_surface.GetNumberOfPoints()):
            hints_index_array.SetTuple(i, [0] * hints_num_max)

        hints_index_array_pre = pre_surface.GetPointData().GetArray("hints_index_array")
        hints_index_array_intra = intra_surface.GetPointData().GetArray("hints_index_array")
        hints_index_array_intra_partial = partialDeformedSurface.GetPointData().GetArray("hints_index_array")
        point_index_intra = partialDeformedSurface.GetPointData().GetArray("pointindex")

        # p_list = []
        # for i in range(0, partialDeformedSurface.GetNumberOfPoints()):
        #     p = int(point_index_intra.GetTuple1(i))
        #     hints_intra = hints_index_array_intra_partial.GetTuple(i)
        #     # print(p)
        #     if not p in p_list:
        #         p_list.append(p)
                
        #         # print(hints_intra)
        #         hints_index_array_pre.SetTuple(p, hints_intra) 
        #         hints_index_array_intra.SetTuple(p, hints_intra)
        #     else:
        #         hints_old = hints_index_array_pre.GetTuple(p) 
        #         hints_new = [a or b for a, b in zip(hints_old, hints_intra)]
        #         hints_index_array_pre.SetTuple(p, hints_new) 
        #         hints_index_array_intra.SetTuple(p, hints_new)

        locator = vtk.vtkPointLocator()
        locator.SetDataSet(intra_surface)
        locator.Update()
        
        # For each point on the partial intraoperative surface, find the corresponding closest point(s)
        # on the deformed complete surface
        for i in range(0, partialDeformedSurface.GetNumberOfPoints()):
            radius = 0.008
            cell_pts_idx = vtk.vtkIdList()
            hints_intra = hints_index_array_intra_partial.GetTuple(i)
            cell_pts_valid = False
            while not cell_pts_valid:
                locator.FindPointsWithinRadius( radius, partialDeformedSurface.GetPoint(i), cell_pts_idx)
                # print("number of nearest points:", cell_pts_idx.GetNumberOfIds())
                if cell_pts_idx.GetNumberOfIds() == 0:
                    # print("number of nearest points:", cell_pts_idx.GetNumberOfIds())
                    radius += 0.001
                    # print("could not find the closet point(s) for point {}, increase radius to {} and re-generate...".format(i, radius))
                else:
                    cell_pts_valid = True

            for j in range(cell_pts_idx.GetNumberOfIds()):
                # grid_array.SetTuple1(cell_pts_idx.GetId(j), field_value)
                p_id = cell_pts_idx.GetId(j)
                hints_old = hints_index_array_pre.GetTuple(p_id) 
                hints_new = [a or b for a, b in zip(hints_old, hints_intra)]
                # if any(hints_new):
                #     print(hints_new)
                hints_index_array_pre.SetTuple(p_id, hints_new) 
                hints_index_array_intra.SetTuple(p_id, hints_new)

        save_debug = True
        if save_debug:
            hints_index_array_list = []
            for idx_h in range(0, 4):
                hints_index_array = vtk.vtkIntArray()
                hints_index_array.SetNumberOfComponents(1)  # length of a tuple
                hints_index_array.SetNumberOfTuples(pre_surface.GetNumberOfPoints())  # length of this array (how many tuples)
                hints_index_array.SetName("hints_index_array_{}".format(idx_h))
                if pre_surface.GetPointData().HasArray("hints_index_array_{}".format(idx_h)):
                    pre_surface.GetPointData().RemoveArray("hints_index_array_{}".format(idx_h))
                pre_surface.GetPointData().AddArray(hints_index_array)
                for i in range(0, pre_surface.GetNumberOfPoints()):
                    hints_index_array.SetTuple1(i, int(hints_index_array_pre.GetTuple(i)[idx_h]))
                hints_index_array_list.append(hints_index_array)
            
            # for idx_i in range(0, hints_index_array_pre.GetNumberOfPoints()):
            #     a_tuple = hints_index_array_pre.GetTuple(idx_i)
            #     for idx_t in len(0, len(a_tuple)):
            #         hints_index_array_list[idx_t].SetTuple1()

        writer = vtk.vtkXMLPolyDataWriter()
        # writer.SetFileName(os.path.join(cur_dir, "partialDeformedSurface_hints.vtp"))
        if weighted_cp:
            pre_surface_output_path = os.path.join(cur_dir, "initSurface_hints_withDispl_deformed.vtp")
        else:
            pre_surface_output_path = os.path.join(cur_dir, "initSurface_hints.vtp")
        writer.SetFileName(pre_surface_output_path)
        writer.SetInputData(pre_surface)
        writer.Update()
        print("{} saved".format(pre_surface_output_path))

        writer = vtk.vtkXMLPolyDataWriter()
        writer.SetFileName(os.path.join(cur_dir, "deformedSurface_hints.vtp"))
        writer.SetInputData(intra_surface)
        writer.Update()
        print("{} saved".format(os.path.join(cur_dir, "deformedSurface_hints.vtp")))
        
        return True

    except Exception as e:
        traceback.print_exc()
        return False



def mark_hint_pre_via_mesh(cur_dir, pre_surface, deformed_surface, hints_mesh_list, output_prefix=None):
    """
        Mark hints on the preoperative surface using hints meshes instead of hints index array stored in the partial
        intraoperative surface
    """
    hints_index_array = vtk.vtkIntArray()
    hints_index_array.SetNumberOfComponents(4)  
    hints_index_array.SetNumberOfTuples(pre_surface.GetNumberOfPoints())  # length of this array (how many tuples)
    hints_index_array.SetName("hints_index_array")
    if pre_surface.GetPointData().HasArray("hints_index_array"): # original hints_index_array has been modified, be careful when saving pre_surface
        pre_surface.GetPointData().RemoveArray("hints_index_array")
    pre_surface.GetPointData().AddArray(hints_index_array)
    for i in range(0, pre_surface.GetNumberOfPoints()):
        hints_index_array.SetTuple(i, [0] * 4)
    
    locator = vtk.vtkPointLocator()
    locator.SetDataSet(deformed_surface)
    locator.Update()
    hints_pre_list = []
    for idx_h, hint_intra in enumerate(hints_mesh_list):
        hint_pre_coord_list = []
        hint_pre_id_list = []
        for idx_hp in range(hint_intra.GetNumberOfPoints()):
            radius = 0.0082
            cell_pts_idx = vtk.vtkIdList()
            cell_pts_valid = False
            while not cell_pts_valid:
                locator.FindPointsWithinRadius( radius, hint_intra.GetPoint(idx_hp), cell_pts_idx)
                # print("number of nearest points:", cell_pts_idx.GetNumberOfIds())
                if cell_pts_idx.GetNumberOfIds() == 0:
                    # print("number of nearest points:", cell_pts_idx.GetNumberOfIds())
                    radius += 0.001
                    # print("could not find the closet point(s) for point {}, increase radius to {} and re-generate...".format(i, radius))
                else:
                    cell_pts_valid = True
            for idx_cp in range(cell_pts_idx.GetNumberOfIds()):
                p_id = cell_pts_idx.GetId(idx_cp)
                if not p_id in hint_pre_id_list:
                    hint_pre_id_list.append(p_id)
        hint_pre_coord_list = [pre_surface.GetPoint(p_id) for p_id in hint_pre_id_list] 
        # print("pre hint {}:".format(idx_h), hint_pre_coord_list)
        hints_pre_list.append(hint_pre_coord_list)
                # hint_pre_coord_list.append(pre_surface.GetPoint(p_id))
            # print("number of closest points:", cell_pts_idx.GetNumberOfIds())
    if output_prefix:
        for idx_h in range(len(hints_pre_list)):
            pre_hint_poly = vtkutils.polyDataToUnstructuredGrid(vtkutils.convert_point_coords_2_poly_1(hints_pre_list[idx_h]))
            output_path = os.path.join(cur_dir, output_prefix.format(idx_h))
            vtkutils.writeMesh(pre_hint_poly, output_path, )
            print("saved preoperative hints to:", output_path)

    return [vtkutils.polyDataToUnstructuredGrid(vtkutils.convert_point_coords_2_poly_1(hint_pre_coord)) for hint_pre_coord in hints_pre_list]



def hints_value_to_grid(cur_dir, surface=None, voxelized_grid=None, tf_flag=False, preop_flag=True):# , partialdeformed_surface=None ):
    """
        Spread 0/1 hints mask (no distance field calculation involved) of preoperative and intraoperative surface onto the voxel grid 
        by looking up the closest point on the voxel grid to the surfaces.
        
    """
    try:
        hints_num_max = 4

        if preop_flag:
            print("Preoperative hints value to grid")
        else:
            print("Introperative hints value to grid")
        
        if not surface:
            if preop_flag:
                surface = vtkutils.loadMesh(os.path.join(cur_dir, "initSurface_hints.vtp"))
            else:
                surface = vtkutils.loadMesh(os.path.join(cur_dir, "partialDeformedSurface.vtp"))
                # surface = vtkutils.loadMesh(os.path.join(cur_dir, "deformedSurface_hints.vtp"))

        if not voxelized_grid:
            if not os.path.exists(os.path.join(cur_dir, "voxelized.vts")):
                # print("path not exists")
                raise ValueError("Path Not Exists{}".format(os.path.join(cur_dir, "voxelized.vts")))
                
            voxelized_grid = vtkutils.loadStructuredGrid(os.path.join(cur_dir, "voxelized.vts"))
        else:
            voxelized_grid = voxelized_grid

        if tf_flag:
            tf = read_tf_matrix(cur_dir=cur_dir, grid=voxelized_grid)

            tfFilter = vtk.vtkTransformFilter()
            tfFilter.SetTransform(tf)
            tfFilter.SetInputData(surface)
            tfFilter.Update()
            surface = tfFilter.GetOutput()
            # surface.GetBounds(bounds)
            # print("bounds:", bounds)

        #voxelized_grid = vtkutils.spreadValuesOnGridPoints(surface, voxelized_grid, "hints_index", radius=0.0)

        # hints_index_voxelized = vtk.vtkDoubleArray()
        # hints_index_voxelized.SetNumberOfComponents(1)
        # hints_index_voxelized.SetName("hints_index_pre" if preop_flag else "hints_index_intra")
        # hints_index_voxelized.SetNumberOfTuples(voxelized_grid.GetNumberOfPoints())
        # hints_index_voxelized.Fill(0.0)

        hints_index_array_voxelized = vtk.vtkIntArray()
        hints_index_array_voxelized.SetNumberOfComponents(hints_num_max)  # length of a tuple
        hints_index_array_voxelized.SetNumberOfTuples(voxelized_grid.GetNumberOfPoints())  # length of this array (how many tuples)
        hints_index_array_voxelized.SetName("hints_index_array_pre" if preop_flag else "hints_index_array_intra")
        for i in range(0, voxelized_grid.GetNumberOfPoints()):
            hints_index_array_voxelized.SetTuple(i, [0] * hints_num_max)

        # hints_index = surface.GetPointData().GetArray("hints_index")
        hints_index_array = surface.GetPointData().GetArray("hints_index_array")
        preoperative = voxelized_grid.GetPointData().GetArray("preoperativeSurface")
        # print(hints_index_array.GetTuple(1))

        pointLocator = vtk.vtkPointLocator()
        pointLocator.SetDataSet(voxelized_grid)
        pointLocator.SetNumberOfPointsPerBucket(1)
        pointLocator.BuildLocator()
        # print(1111)
        mesh2IDs = []

        for idx in range(surface.GetNumberOfPoints()):
            mesh1Point = surface.GetPoint(idx)
            mesh2PointID = pointLocator.FindClosestPoint(mesh1Point)
            # hints_index_voxelized.SetTuple(mesh2PointID, hints_index.GetTuple(idx))
            # if hints_index_array.GetTuple(idx) != (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0):
            #     print(hints_index_array.GetTuple(idx))
            
            #print("surface:{}, grid:{}".format(mesh1Point, voxelized_grid.GetPoint(mesh2PointID)))

            # print("voxel preoperative value:", preoperative.GetTuple1(mesh2PointID))
            hints_to_be_added = hints_index_array.GetTuple(idx)
            if not any(hints_to_be_added):
                continue
            else:
                if mesh2PointID in mesh2IDs:
                    # pass
                    hints_old = hints_index_array_voxelized.GetTuple(mesh2PointID) 
                    hints_new = [a or b for a, b in zip(hints_old, hints_to_be_added)]
                    hints_index_array_voxelized.SetTuple(mesh2PointID, hints_new)
                else:
                    mesh2IDs.append(mesh2PointID)
                    hints_index_array_voxelized.SetTuple(mesh2PointID, hints_to_be_added)

        if voxelized_grid.GetPointData().HasArray("hints_index_pre" if preop_flag else "hints_index_intra"):
            voxelized_grid.GetPointData().RemoveArray("hints_index_pre" if preop_flag else "hints_index_intra")
        if voxelized_grid.GetPointData().HasArray("hints_index_array_pre" if preop_flag else "hints_index_array_intra"):
            voxelized_grid.GetPointData().RemoveArray("hints_index_array_pre" if preop_flag else "hints_index_intra")
        # voxelized_grid.GetPointData().AddArray(hints_index_voxelized)
        voxelized_grid.GetPointData().AddArray(hints_index_array_voxelized)
        # print("closest point list length:{}, surface point number:{}".format(len(mesh2IDs), surface.GetNumberOfPoints()))
        if preop_flag:
            print("preoperative surface points number:{}, voxel with hints:{}".format(surface.GetNumberOfPoints(), len(mesh2IDs)))
        else:
            print("intraoperative surface points number:{}, voxel with hints:{}".format(surface.GetNumberOfPoints(), len(mesh2IDs)))

        writer = vtk.vtkXMLStructuredGridWriter()
        writer.SetFileName(os.path.join(cur_dir, "voxelized.vts"))
        writer.SetInputData(voxelized_grid)
        writer.Update()
        print("File:{} saved".format(os.path.join(cur_dir, "voxelized.vts")))
        return True
    except Exception as e:
        traceback.print_exc()
        return False


def read_tf_matrix(cur_dir, grid=None):
    """
        Load transformation matrix stored in the voxel grid
    """
    try:
        if not grid:
            grid = vtkutils.loadStructuredGrid(os.path.join(cur_dir, "voxelized.vts"))
        else:
            grid = grid

        matArray = grid.GetFieldData().GetArray( "TransformationMatrix" )
        if matArray:
            tf = vtk.vtkTransform()
            mat = vtk.vtkMatrix4x4()
            for i in range( 0, 16 ):
                val = matArray.GetTuple1( i )
                mat.SetElement( int(i/4), i % 4, val )
            tf.SetMatrix( mat )
            return tf
        else:
            raise IOError("No 'TransformationMatrix' array found in field data.")
    except Exception as e:
        traceback.print_exc()
        return False



def hints_index_array_to_DF(cur_dir, initSurface=None, partialDeformedSurface=None, voxel_grid=None, pre_to_DF=False, output_filename="voxelized.vts"):
    """
        Voxelize matching cues (displacement hints)
    """
    try:
        print("hints index array to DF:------------")
        if not initSurface:
            initSurface = vtkutils.loadMesh(os.path.join(cur_dir, "initSurface_hints.vtp"))
        else:
            initSurface = initSurface
        if not partialDeformedSurface:
            partialDeformedSurface = vtkutils.loadMesh(os.path.join(cur_dir, "partialDeformedSurface.vtp"))
        else:
            partialDeformedSurface = partialDeformedSurface
        if not voxel_grid:
            # if os.path.exists(os.path.join(cur_dir, "voxelized.vts"))
            voxel_grid = vtkutils.loadStructuredGrid(os.path.join(cur_dir, "voxelized.vts"))
        else:
            voxel_grid = voxel_grid

        hints_index_array_intra = partialDeformedSurface.GetPointData().GetArray("hints_index_array")
        hints_index_array_pre = initSurface.GetPointData().GetArray("hints_index_array")

        hints_index_array_intra_single = vtk.vtkFloatArray()
        hints_index_array_intra_single.SetNumberOfComponents(1)  # length of a tuple
        hints_index_array_intra_single.SetNumberOfTuples(partialDeformedSurface.GetNumberOfPoints())  # length of this array (how many tuples)
        hints_index_array_intra_single.SetName("hints_index_array_intra_single")
   
        hints_index_array_pre_single = vtk.vtkFloatArray()
        hints_index_array_pre_single.SetNumberOfComponents(1)  # length of a tuple
        hints_index_array_pre_single.SetNumberOfTuples(initSurface.GetNumberOfPoints())  # length of this array (how many tuples)
        hints_index_array_pre_single.SetName("hints_index_array_pre_single")
        
        partialDeformedSurface.GetPointData().AddArray(hints_index_array_intra_single)
        initSurface.GetPointData().AddArray(hints_index_array_pre_single)
 
        tf = read_tf_matrix(cur_dir=cur_dir, grid=voxel_grid)

        if pre_to_DF:
            import preprocess_deform
            voxel_grid = preprocess_deform.voxelization_warped_pre_surface(cur_dir=cur_dir, voxel_grid=voxel_grid, tf=tf)

        for i in range(0, 4):
            print("hints intra:")
            c_intra = 0
            for id in range(0, partialDeformedSurface.GetNumberOfPoints()):
                hints_index_tuple_intra = hints_index_array_intra.GetTuple(id)
                # print(hints_index_tuple_intra[0])
                hints_index_array_intra_single.SetTuple1(id, hints_index_tuple_intra[i])
                if hints_index_tuple_intra[i] == 1:
                    c_intra += 1
                    hints_index_array_intra_single.SetTuple1(id, 1)

            thresh = vtk.vtkThresholdPoints()
            thresh.SetInputData( partialDeformedSurface )
            thresh.SetInputArrayToProcess(0, 0, vtk.vtkDataObject.FIELD_ASSOCIATION_POINTS, vtk.vtkDataSetAttributes.SCALARS, "hints_index_array_intra_single" )
            thresh.ThresholdByUpper(0.9)
            thresh.Update()

            partialSurface = vtkutils.unstructuredGridToPolyData( thresh.GetOutput() )
            # print(partialSurface.GetPointData().HasArray("hints_index_array_single"))
            # hints_index_array_single_partial = partialSurface.GetPointData().GetArray("hints_index_array_intra_single")
            # for j in range(0, partialSurface.GetNumberOfPoints()):
            #     print(hints_index_array_single_partial.GetTuple1(j))
            print("{} number of points:".format(i), partialSurface.GetNumberOfPoints())
            print("{} hints larger than 1".format(i), c_intra)

            enable_debug = True
            if enable_debug:
                writer = vtk.vtkXMLPolyDataWriter()
                writer.SetFileName(os.path.join(cur_dir, "hints_intra_test_{}.vtp".format(i)))
                writer.SetInputData(partialSurface)
                writer.Update()

            tfFilter = vtk.vtkTransformFilter()
            tfFilter.SetTransform(tf)
            tfFilter.SetInputData(partialSurface)
            tfFilter.Update()
            partialSurface = tfFilter.GetOutput()

            if voxel_grid.GetPointData().HasArray("hints_index_intra_DF_{}".format(i)):
                voxel_grid.GetPointData().RemoveArray("hints_index_intra_DF_{}".format(i))
            voxelize.distanceFieldGPU(surface=partialSurface, targetGrid=voxel_grid, targetArrayName="hints_index_intra_DF_{}".format(i))

            print("hints pre:")
            c_pre = 0
            for id in range(0, initSurface.GetNumberOfPoints()):
                hints_index_tuple_pre = hints_index_array_pre.GetTuple(id)
                
                if hints_index_tuple_pre[i] == 1:
                    c_pre += 1
                hints_index_array_pre_single.SetTuple1(id, hints_index_tuple_pre[i])

            thresh = vtk.vtkThresholdPoints()
            thresh.SetInputData( initSurface )
            thresh.SetInputArrayToProcess(0, 0, vtk.vtkDataObject.FIELD_ASSOCIATION_POINTS, vtk.vtkDataSetAttributes.SCALARS, "hints_index_array_pre_single" )
            thresh.ThresholdByUpper(0.9)
            thresh.Update()

            partialSurface = vtkutils.unstructuredGridToPolyData( thresh.GetOutput() )
            print("{} number of points:".format(i), partialSurface.GetNumberOfPoints())
            print("{} hints larger than 1".format(i), c_pre)

            enable_debug = True
            if enable_debug:
                writer = vtk.vtkXMLPolyDataWriter()
                writer.SetFileName(os.path.join(cur_dir, "hints_pre_test_{}.vtp".format(i)))
                writer.SetInputData(partialSurface)
                writer.Update()

            tfFilter = vtk.vtkTransformFilter()
            tfFilter.SetTransform(tf)
            tfFilter.SetInputData(partialSurface)
            tfFilter.Update()
            partialSurface = tfFilter.GetOutput()

            # voxelize.distanceField(surfaceMesh=partialSurface, targetGrid=voxel_grid, targetArrayName="hints_index_pre_DF_{}".format(i))
            if voxel_grid.GetPointData().HasArray("hints_index_pre_DF_{}".format(i)):
                voxel_grid.GetPointData().RemoveArray("hints_index_pre_DF_{}".format(i))
            voxelize.distanceFieldGPU(surface=partialSurface, targetGrid=voxel_grid, targetArrayName="hints_index_pre_DF_{}".format(i))

        writer = vtk.vtkXMLStructuredGridWriter()
        writer.SetFileName(os.path.join(cur_dir, output_filename))
        writer.SetInputData(voxel_grid)
        writer.Update()
        print("File:{} saved".format(os.path.join(cur_dir, output_filename)))

        return True
    except Exception as e:
        traceback.print_exc()
        return False


def calculate_stats_hints_size(cur_dir,):
    try:
        print("calculate hints size")
        pre_surface = vtkutils.loadMesh(os.path.join(cur_dir, "initSurface_hints_withDispl_deformed.vtp"))

        for idx in range(0, 4):
            thresh = vtk.vtkThresholdPoints()
            thresh.SetInputData( pre_surface )
            thresh.SetInputArrayToProcess(0, 0, vtk.vtkDataObject.FIELD_ASSOCIATION_POINTS, vtk.vtkDataSetAttributes.SCALARS, "hints_index_array_intra_single" )
            thresh.ThresholdByUpper(0.9)
            thresh.Update()

    except Exception as e:
        traceback.print_exc()


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="Extract partial deformed surfaces as hints.")
    parser.add_argument("datapath", type=utils.path, help="Folder(s) in which the samples lie")
    parser.add_argument("--num", type=int, default=1000, help="Amount of data samples to load from [dataFolder]")
    parser.add_argument("--start", type=int, default=0, help="Start number of folder")
    parser.add_argument("--folder_len", type=int, default=6, help="Start number of folder")
    # parser.add_argument("--outputFolder", required=True, type=string, help="Folder in which to save the results and models")
    args = parser.parse_args()
    data_path = args.datapath
    num = args.num
    start = args.start
    folder_len = args.folder_len
    

    try:
        if not os.path.exists(data_path):
            print("path {} not exists".format(data_path))
            sys.exit(0)

        data_list = list(filter(lambda x: os.path.isdir(os.path.join(data_path, x)), os.listdir(data_path)))
        if len(data_list) < num:
            print("The given data number larger than the number of actually existing data, set to maximum:{}".format(
                len(data_list)))
            num = len(data_list)

        end = start + num

        for p in range(start, end):
            if folder_len == 5:
                cur_dir = os.path.join(data_path, "{:05d}".format(p))
            elif folder_len == 6:
                cur_dir = os.path.join(data_path, "{:06d}".format(p))
            if not os.path.exists(cur_dir):
                print("path:{} not exists, task may be done".format(cur_dir))
                continue
            print("====================Dealing with directory:{}========================".format(cur_dir))
            # extract hints on partial intraoperative hints:
            # if not extract_partial_surfaces_intraop(cur_dir, hints_shape="round"):
            if not extract_partial_surfaces_intraop(cur_dir, weighted_cp=True):
                print("Error occurs when extract hints for {}".format(cur_dir))
                break

            # hints to preoperative and intraoperative surface:
            # pre load deformed preoperative surface 
            # if not os.path.exists(os.path.join(cur_dir, "initSurface_hints_withDispl_deformed.vtu")):
            #     print("{} not exists, go next".format(os.path.join(cur_dir, "initSurface_hints_withDispl_deformed.vtu")))
            #     continue
            if not os.path.exists(os.path.join(cur_dir, "volume_withDispl_deformed.vtu")):
                print("{} not exists".format(os.path.join(cur_dir, "volume_withDispl_deformed.vtu")))
                break
                # continue
            # pre_surface = vtkutils.loadMesh(os.path.join(cur_dir, "initSurface_hints_withDispl_deformed.vtu"))
            # pre_surface = vtkutils.unstructuredGridToPolyData(pre_surface)
            # vtkutils.writeMesh(pre_surface, os.path.join(cur_dir, "initSurface_hints_withDispl_deformed.vtp"))
            # intra_surface = vtkutils.loadMesh(os.path.join(cur_dir, "deformedSurface_hints.vtp"))
            pre_volume_deformed = vtkutils.loadMesh(os.path.join(cur_dir, "volume_withDispl_deformed.vtu"))
            pre_surface = vtkutils.extractSurface(pre_volume_deformed)
            pre_surface = vtkutils.unstructuredGridToPolyData(pre_surface)
            vtkutils.writeMesh(pre_surface, os.path.join(cur_dir, "initSurface_withDispl_deformed.vtp"))
            # intra_surface = vtkutils.loadMesh(os.path.join(cur_dir, "deformedSurface_hints.vtp"))
            # if not mark_hints_pre(cur_dir):
            if not mark_hints_pre(cur_dir, pre_surface=pre_surface, weighted_cp=True):
                print("Error occurs when mark hints for pre-op surface {}".format(cur_dir))
                break
                # continue

            # hints to grid (0-1 mask none DF):
            # if not (hints_value_to_grid(cur_dir, surface=None, tf_flag=True, preop_flag=True) and \
            #     hints_value_to_grid(cur_dir, surface=None, tf_flag=True, preop_flag=False)):
            #     print("Integrate hints data to voxel grid {}failed!".format(cur_dir))

            # check before converting hints mask to DF:
            # if not os.path.exists(os.path.join(cur_dir, "initSurface_hints.vtp")) and not os.path.exists(os.path.join(cur_dir, "deformedSurface_hints.vtp")):
            #     if os.path.exists(os.path.join(cur_dir, "voxelized.vts")): # but voxel grid was generated
            #         print("files missing!")
            #         extract_partial_surfaces_intraop(cur_dir)
            #         mark_hints_pre(cur_dir)
            #         hints_value_to_grid(cur_dir, surface=None, tf_flag=True, preop_flag=True)
            #         hints_value_to_grid(cur_dir, surface=None, tf_flag=True, preop_flag=False)
            #         # break
            # else:
            #     print("files compelete")

            # hints to grid DF (convert mask to DF):
            if not os.path.exists(os.path.join(cur_dir, "initSurface_hints_withDispl_deformed.vtp")):
                print("{} not exists".format(os.path.join(cur_dir, "initSurface_hints_withDispl_deformed.vtp")))
                break
            else:
                initSurface = vtkutils.loadMesh(os.path.join(cur_dir, "initSurface_hints_withDispl_deformed.vtp"))
            if not hints_index_array_to_DF(cur_dir, initSurface=initSurface):
                print("Error occurs when converting hints representation from mask to DF {}".format(cur_dir))
                break
    except Exception as e:
        traceback.print_exc()

