import traceback
from numpy.core.fromnumeric import trace
from numpy.lib.npyio import load
from vtk import *
import vtk
from vtk.util import numpy_support
import numpy as np
import math
import sys
import os
import random

sys.path.append(os.path.join(os.path.dirname(
    os.path.abspath(__file__)), "Utils"))


def loadMesh(filename):
    """
    Loads a mesh using VTK. Supported file types: stl, ply, obj, vtk, vtu, vtp, pcd.

    Arguments:
    ---------
    filename (str)

    Returns:
    --------
    vtkDataSet
            which is a vtkUnstructuredGrid or vtkPolyData, depending on the file type of the mesh.
    """

    # Load the input mesh:
    fileType = filename[-4:].lower()
    if fileType == ".stl":
        reader = vtkSTLReader()
        reader.SetFileName(filename)
        reader.Update()
        mesh = reader.GetOutput()
    elif fileType == ".obj":
        reader = vtkOBJReader()
        reader.SetFileName(filename)
        reader.Update()
        mesh = reader.GetOutput()
    elif fileType == ".ply":
        reader = vtkPLYReader()
        reader.SetFileName(filename)
        reader.Update()
        mesh = reader.GetOutput()
    elif fileType == ".vtk":
        reader = vtkUnstructuredGridReader()
        reader.SetFileName(filename)
        reader.Update()
        mesh = reader.GetOutput()
    elif fileType == ".vtu":
        reader = vtkXMLUnstructuredGridReader()
        reader.SetFileName(filename)
        reader.Update()
        mesh = reader.GetOutput()
    elif fileType == ".vtp":
        reader = vtkXMLPolyDataReader()
        reader.SetFileName(filename)
        reader.Update()
        mesh = reader.GetOutput()
    elif fileType == ".pcd":
        import pcl
        pc = pcl.load(filename)
        pts = vtkPoints()
        verts = vtkCellArray()
        for i in range(pc.size):
            pts.InsertNextPoint(pc[i][0], pc[i][1], pc[i][2])
            verts.InsertNextCell(1, (i,))
        mesh = vtkPolyData()
        mesh.SetPoints(pts)
        mesh.SetVerts(verts)

    else:
        raise IOError(
            "Mesh should be .vtk, .vtu, .vtp, .obj, .stl, .ply or .pcd file!")

    if mesh.GetNumberOfPoints() == 0:
        raise IOError("Could not load a valid mesh from {}".format(filename))
    return mesh


def writeMesh(mesh, filename, verbose=False):
    """
    Saves a VTK mesh to file. 
    Supported file types: stl, ply, obj, vtk, vtu, vtp, pcd, vts.

    Arguments:
    ---------
    mesh (vtkDataSet):
            mesh to save
    filename (str): 
            name of the file where to save the input mesh. MUST contain the desired extension.

    """
    if verbose:
        print("writing mesh to", filename)
    if mesh.GetNumberOfPoints() == 0:
        raise IOError("Input mesh has no points!")

    # Get file format
    fileType = filename[-4:].lower()
    if fileType == ".stl":
        writer = vtkSTLWriter()
        writer.SetFileName(filename)
        writer.SetInputData(mesh)
        writer.Update()
    elif fileType == ".obj":
        writer = vtkOBJWriter()
        writer.SetFileName(filename)
        writer.SetInputData(mesh)
        writer.Update()
    elif fileType == ".ply":
        writer = vtkPLYWriter()
        writer.SetFileName(filename)
        writer.SetInputData(mesh)
        writer.Update()
    elif fileType == ".vtk":
        writer = vtkUnstructuredGridWriter()
        writer.SetFileName(filename)
        writer.SetInputData(mesh)
        writer.Update()
    elif fileType == ".vtu":
        writer = vtkXMLUnstructuredGridWriter()
        writer.SetFileName(filename)
        writer.SetInputData(mesh)
        writer.Update()
    elif fileType == ".vts":
        writer = vtkXMLStructuredGridWriter()
        writer.SetFileName(filename)
        writer.SetInputData(mesh)
        writer.Update()
    elif fileType == ".vtp":
        writer = vtkXMLPolyDataWriter()
        writer.SetFileName(filename)
        writer.SetInputData(mesh)
        writer.Update()
    else:
        raise IOError(
            "Supported extensions are .vtk, .vtu, .vts, .vtp, .obj, .stl, .ply!")


def loadStructuredGrid(filename):
    """
    Loads a structured grid using VTK. Supported file types: vtk, vts.

    Arguments:
    ---------
    filename (str)

    Returns:
    --------
    vtkDataSet
            of type vtkStructuredGrid.
    """
    fileType = filename[-4:].lower()
    if fileType == ".vtk":
        reader = vtkStructuredGridReader()
        reader.SetFileName(filename)
        reader.Update()
        grid = reader.GetOutput()
    elif fileType == ".vts":
        reader = vtkXMLStructuredGridReader()
        reader.SetFileName(filename)
        reader.Update()
        grid = reader.GetOutput()
    else:
        raise IOError(filename + " should be .vtk or .vts")
    return grid





def storeTransformationMatrix( grid, tf ):
    mat = tf.GetMatrix()
    matArray = vtk.vtkFloatArray()
    matArray.SetNumberOfTuples(16)
    matArray.SetNumberOfComponents(1)
    matArray.SetName( "TransformationMatrix" )
    for row in range(0,4):
        for col in range(0,4):
            matArray.SetTuple1( row*4+col, mat.GetElement( row, col ) )
    grid.GetFieldData().AddArray(matArray)
    return True


def scale_model(model, scale_matrix=(1,1,1), store_transform=False):

    transform = vtk.vtkTransform()
    transform.Scale(scale_matrix)

    transformFilter = vtk.vtkTransformPolyDataFilter()
    transformFilter.SetInputData(model)
    transformFilter.SetTransform(transform)
    transformFilter.Update()

    model_scaled = transformFilter.GetOutput()
    print("model scaled by matrix:", scale_matrix)
    if store_transform:
        storeTransformationMatrix(grid=model, tf=transform)
    return model_scaled


def center_mesh(mesh):
    tf = vtk.vtkTransform()
    bounds = [0]*6;
    mesh.GetBounds(bounds)
    dx = -(bounds[1]+bounds[0])*0.5
    dy = -(bounds[3]+bounds[2])*0.5
    dz = -(bounds[5]+bounds[4])*0.5
    print("Moving point cloud by:", (dx,dy,dz) )
    tf.Translate( (dx,dy,dz) )

    tfFilter = vtk.vtkTransformFilter()
    tfFilter.SetTransform( tf )
    tfFilter.SetInputData( mesh )
    tfFilter.Update()
    mesh = tfFilter.GetOutput()
    return mesh



def extractSurface(inputMesh):
    surfaceFilter = vtkDataSetSurfaceFilter()
    surfaceFilter.SetInputData(inputMesh)
    surfaceFilter.Update()
    surface = surfaceFilter.GetOutput()

    return surface



class ErrorObserver:

    def __init__(self):
        self.__ErrorOccurred = False
        self.__ErrorMessage = None
        self.CallDataType = 'string0'

    def __call__(self, obj, event, message):
        self.__ErrorOccurred = True
        self.__ErrorMessage = message

    def ErrorOccurred(self):
        occ = self.__ErrorOccurred
        self.__ErrorOccurred = False
        return occ

    def ErrorMessage(self):
        return self.__ErrorMessage


def makeSingleFloatArray(name, val):
    arr = vtkFloatArray()
    arr.SetNumberOfTuples(1)
    arr.SetNumberOfComponents(1)
    arr.SetTuple1(0, val)
    arr.SetName(name)
    return arr


def surfaceArea(mesh):
    area = 0
    for i in range(mesh.GetNumberOfCells()):
        if mesh.GetCell(i).GetCellType() == VTK_TRIANGLE:
            p0 = mesh.GetCell(i).GetPoints().GetPoint(0)
            p1 = mesh.GetCell(i).GetPoints().GetPoint(1)
            p2 = mesh.GetCell(i).GetPoints().GetPoint(2)
            a = mesh.GetCell(i).TriangleArea(p0, p1, p2)
            area += a

    areaArr = makeSingleFloatArray("surfaceArea", area)
    mesh.GetFieldData().AddArray(areaArr)

    return area



def to_numpy( data, array_name=None, shape=None ):
    """
    Convert point array or coordinates to numpy array
    e.g. to_numpy(surface.GetPoints().GetData())

    """
    from vtk.util.numpy_support import vtk_to_numpy, numpy_to_vtk
    if array_name:
        arr = vtk_to_numpy( data.GetArray( array_name ) )
    else:
        arr = vtk_to_numpy(data)

    if shape:
        arr = arr.reshape( shape )
    return arr    


def unstructuredGridToPolyData(ug):
    """
    Converts an input unstructured grid into a polydata object. 
    Be careful since PolyData objects cannot contain 3D elements, thus all 
    tetrahedra will be lost with this operation.

    Parameters
    ----------
    ug (vtkUnstructuredGrid):
            The input unstructured grid

    Returns
    ----------
    vtkPolyData

    """
    geometryFilter = vtkGeometryFilter()
    geometryFilter.SetInputData(ug)
    geometryFilter.Update()
    return geometryFilter.GetOutput()


def polyDataToUnstructuredGrid(pd):
    """
    Converts an input polydata into an unstructured grid.

    Parameters
    ----------
    pd (vtkPolyData):
            The input polydata

    Returns
    ----------
    vtkUnstructuredGrid

    """
    appendFilter = vtkAppendFilter()
    appendFilter.SetInputData(pd)
    appendFilter.Update()
    return appendFilter.GetOutput()




def applyTransform(mesh, vtkTransform):
    tf = vtkTransformFilter()
    tf.SetInputData(mesh)
    tf.SetTransform(vtkTransform)
    tf.Update()
    mesh = tf.GetOutput()
    return mesh



def removeDoubleFaces(surfaceMesh):
    outputMesh = vtkPolyData()
    outputPoints = vtkPoints()
    outputPoints.DeepCopy(surfaceMesh.GetPoints())
    outputMesh.SetPoints(outputPoints)
    outputCells = vtkCellArray()
    outputMesh.SetPolys(outputCells)

    copied = {}
    skipped = 0

    for i in range(surfaceMesh.GetNumberOfCells()):
        c = surfaceMesh.GetCell(i)
        if c.GetCellType() == VTK_TRIANGLE:
            pIds = c.GetPointIds()
            idList = [pIds.GetId(i) for i in range(pIds.GetNumberOfIds())]
            idList = sorted(idList)
            idListStr = ",".join(str(x) for x in idList)
            #print(idList, idListStr)
            if not idListStr in copied:
                copied[idListStr] = True
                outputCells.InsertNextCell(3, idList)
            else:
                skipped += 1

    return outputMesh



def getCellNeighbors(mesh, cellId):

    cell = mesh.GetCell(cellId)

    neighbors = []

    idList = vtkIdList()

    for i in range(cell.GetNumberOfEdges()):
        edge = cell.GetEdge(i).GetPointIds()
        mesh.GetCellNeighbors(cellId, edge, idList)
        neighbors += [idList.GetId(i) for i in range(idList.GetNumberOfIds())]

    return neighbors



def loadTransformationMatrix( grid ):
    matArray = grid.GetFieldData().GetArray( "TransformationMatrix" )
    if matArray:
        tf = vtkTransform()
        mat = vtkMatrix4x4()
        for i in range( 0, 16 ):
            val = matArray.GetTuple1( i )
            mat.SetElement( int(i/4), i % 4, val )
        tf.SetMatrix( mat )
        return tf
    else:
        raise IOError("No 'TransformationMatrix' array found in field data.")


def applyTransformation( dataset, tf ):
    for i in range( dataset.GetPointData().GetNumberOfArrays() ):
        arr = dataset.GetPointData().GetArray(i)
        if arr.GetNumberOfComponents() == 3:
            for j in range(arr.GetNumberOfTuples()):
                data = arr.GetTuple3(j)
                transformed = tf.TransformVector( data )
                arr.SetTuple3(j,transformed[0],transformed[1],transformed[2])


def decimation(mesh):

    print("Before decimation\n"
          "-----------------\n"
          "There are " + str(mesh.GetNumberOfPoints()) + "points.\n")

    decimate = vtkQuadricDecimation()
    decimate.SetInputData(mesh)
    decimate.SetTargetReduction(0.9)
    decimate.Update()
    # decimate = vtkDecimatePro()
    # decimate.SetInputData(inputPoly)
    # decimate.SetTargetReduction(.10)
    # decimate.Update()

    decimatedPoly = vtkPolyData()
    decimatedPoly.ShallowCopy(decimate.GetOutput())

    print("After decimation \n"
          "-----------------\n"
          "There are " + str(decimatedPoly.GetNumberOfPoints()) + "points.\n")

    return decimatedPoly




# spread arrays from voxel grid to mesh:
# eg. spreading estimated displacement field back to mesh in order to examine the deformation in paraview
# eg. spreading displacement error back to mesh in order to extract displacement hints based on displacement field error
def displ_field_interpolate(mesh, field, output_dir, output_filename=None):
    scale = 1 # default
    try:
        tf = loadTransformationMatrix( field )
        tf.Inverse()
        print("Applying transform")
        tfFilter = vtkTransformFilter()
        tfFilter.SetTransform( tf )
        tfFilter.SetInputData( field )
        tfFilter.Update()
        field = tfFilter.GetOutput()
        
        # Apply transformation also to all vector fields:
        applyTransformation( field, tf )
        
        scale = tf.GetMatrix().GetElement(0,0)
    except Exception as e:
        print(e)
        print("Could not find or apply transformation. Skipping.")


    try:
        # writer = vtkXMLStructuredGridWriter()
        # writer.SetInputData( field )
        # writer.SetFileName( os.path.join( output_dir, "field.vts" ) )
        # writer.Update()
        # print("Written1")

        # Threshold to ignore all points outside of field:
        threshold = vtkThreshold()
        threshold.SetInputArrayToProcess(0, 0, 0, vtkDataObject.FIELD_ASSOCIATION_POINTS, "preoperativeSurface")
        threshold.ThresholdByLower(0)
        threshold.SetInputData( field )
        threshold.Update()
        fieldInternal = threshold.GetOutput()
        
        if fieldInternal.GetNumberOfPoints() == 0:
            print("\033[93m field internal point number is zero, recording and go next....\033[0m")
            with open("/home/liupeng/1_Code/springsimulation_newpipeline/HintsExtraction/list_voxel_without_preop.txt", "a+") as f:
                without_preop_list = f.readlines()
                if not "{}\n".format(os.path.basename(output_dir)) in without_preop_list:
                    f.write("{}\n".format(os.path.basename(output_dir)))
                f.close()
            return None

        print("Scale", scale)

        if mesh.GetPointData().HasArray("estimatedDisplacement"):
            print("found former estimatedDisplacement field, removing...")
            mesh.GetPointData().RemoveArray("estimatedDisplacement")

        kernel = vtkGaussianKernel()
        kernel.SetRadius(0.01*scale) 
        kernel.SetKernelFootprintToRadius()
        #kernel.SetKernelFootprintToNClosest()
        #kernel.SetNumberOfPoints( 4 )

        interpolator = vtkPointInterpolator()
        interpolator.SetKernel( kernel )
        interpolator.SetNullPointsStrategyToMaskPoints()
        interpolator.SetValidPointsMaskArrayName( "validInternalPoints" )
        #interpolator.SetNullPointsStrategyToClosestPoint()
        interpolator.SetSourceData( fieldInternal )
        interpolator.SetInputData( mesh )
        interpolator.Update()
        output = interpolator.GetOutput()

        # writer = vtkXMLUnstructuredGridWriter()
        # writer.SetInputData( fieldInternal )
        # writer.SetFileName( os.path.join( output_dir, "fieldInternal.vtu" ) )
        # writer.Update()
        # print(333)

        append = vtkAppendFilter()
        append.AddInputData( output )
        append.Update()
        output = append.GetOutput()

        # if save == True:
        # if not output_filename:
        #     output_filename = "initSurface_hints_withDispl.vtu"
        # else:
        #     output_filename = output_filename
        print(output_filename)
        if output_filename:
            print("write mesh:", os.path.join( output_dir, output_filename ))
            writeMesh(output, os.path.join( output_dir, output_filename ))
        # writer = vtkXMLUnstructuredGridWriter()
        # writer.SetInputData( output )

        # writer.SetFileName( os.path.join( output_dir, output_filename ) )
        # writer.Update()

        return output
    except Exception as e:
        traceback.print_exc()
        return None


def warp_mesh(mesh_with_displ, displacement_field_name, output_folder, output_filename):
    if not mesh_with_displ.GetPointData().HasArray(displacement_field_name):
        print("mesh don't have the array {}, please check!".format(displacement_field_name))
        return None
    print("warping by vector {}".format(displacement_field_name))
    # hints_array_
    mesh_with_displ.GetPointData().SetActiveVectors(displacement_field_name)
    warpVector = vtkWarpVector()
    warpVector.SetInputData(mesh_with_displ)
    warpVector.Update()
    output_warped = warpVector.GetOutput()

    if output_filename:
        print("writing warped mesh to {}".format(os.path.join( output_folder, output_filename)))
        writeMesh(output_warped, os.path.join( output_folder, output_filename))
    return output_warped


def calc_hausdorff_distance(point_set_0, point_set_1):
    hausdorff_distance = vtk.vtkHausdorffDistancePointSetFilter()
    hausdorff_distance.SetInputData(0, point_set_0)
    hausdorff_distance.SetInputData(1, point_set_1)
    hausdorff_distance.Update()
    return hausdorff_distance.GetHausdorffDistance()


def get_center_of_mass(mesh):
    centerOfMassFilter = vtk.vtkCenterOfMass()
    centerOfMassFilter.SetInputData(mesh)
    centerOfMassFilter.SetUseScalarsAsWeights(False)
    centerOfMassFilter.Update()
    c = centerOfMassFilter.GetCenter()
    return c





def convert_point_coords_2_poly_1(point_coords):
    a = np.asarray(point_coords)

    points = vtk.vtkPoints()
    vertices = vtk.vtkCellArray()

    for p in a:
        # print(p)
        id = points.InsertNextPoint(p)
        vertices.InsertNextCell(1)
        vertices.InsertCellPoint(id)

    # Create a polydata object
    poly = vtk.vtkPolyData()

    # Set the points and vertices we created as the geometry and topology of the polydata
    poly.SetPoints(points)
    poly.SetVerts(vertices)

    # # Visualize
    # mapper = vtk.vtkPolyDataMapper()
    # if vtk.VTK_MAJOR_VERSION <= 5:
    #     mapper.SetInput(point)
    # else:
    #     mapper.SetInputData(point)
    
    return poly






def add_blank_array( mesh, array_name, comp_num=1):
    arr = vtk.vtkDoubleArray()
    arr.SetName(array_name)
    arr.SetNumberOfComponents(comp_num)
    arr.SetNumberOfTuples( mesh.GetNumberOfPoints() )
    mesh.GetPointData().AddArray(arr)
    return mesh


def clean_poly(poly):
    print("before clean:", poly.GetNumberOfPoints())
    cleanPolyData = vtk.vtkCleanPolyData()
    cleanPolyData.SetInputData(poly)
    cleanPolyData.Update()
    poly_cleaned = cleanPolyData.GetOutput()
    print("after clean:", poly_cleaned.GetNumberOfPoints())
    return poly_cleaned



if __name__ == "__main__":


    import argparse
    parser = argparse.ArgumentParser(description="Tests for the vtkutils functions")
    parser.add_argument("datadir", type=str, help="Directory where the voxelize.vts lies")
    args = parser.parse_args()
    

    # test center of mass:
    for idx_s in range(0, 4):
        base_path = "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/data/data_sparsedatachallenge_residual/00000{}/".format(idx_s)
        print("distance between center points of two point sets:", base_path)
        for idx_p in range(0, 3):
            point_set_0_filename = "hints_intra_{}.vtp".format(idx_p)
            point_set_1_filename = "hints_pre_{}.vtk".format(idx_p)
            point_set_0 = loadMesh(os.path.join(base_path, point_set_0_filename))
            point_set_1 = loadMesh(os.path.join(base_path, point_set_1_filename))
            c0 = np.asarray(get_center_of_mass(point_set_0))
            c1 = np.asarray(get_center_of_mass(point_set_1))

            dist = np.linalg.norm(c0 - c1)
            print(dist)

