import os, shutil, argparse, sys
import numpy
import vtk
from vtk import util
# sys.path.append("../")
import vtkutils
import generalutils as utils
import voxelize_displacement
import voxelize
import traceback
from vtk.util.numpy_support import vtk_to_numpy, numpy_to_vtk
import torch



def copy_files(source, target):
    """Function which copy files from original V2S-Net dataset folder to Cue-Net dataset folder.

    Args:
        source (str): source folder
        target (str): target folder

    Returns:
        bool: Return the flag indicates success or failure
    """
    # if os.path.exists(os.path.join(cur_dir, "surface.stl")):
    #     print("copy surface.stl to {}".format(output_dir))
    #     shutil.copy(os.path.join(cur_dir, "surface.stl"), os.path.join(output_dir, "surface.stl"))
    if os.path.exists(os.path.join(source, "partialDeformedSurface.vtp")):
        print("copy files to {}".format(target))
        shutil.copy(os.path.join(source, "partialDeformedSurface.vtp"), os.path.join(target, "partialDeformedSurface.vtp"))
        # shutil.copy(os.path.join(cur_dir, "initSurface_hints.vtp"), os.path.join(output_dir, "initSurface_hints.vtp"))
        # shutil.copy(os.path.join(cur_dir, "deformedSurface_hints.vtp"), os.path.join(output_dir, "deformedSurface_hints.vtp"))
        shutil.copy(os.path.join(source, "deformed.vtu"), os.path.join(target, "deformed.vtu"))
        shutil.copy(os.path.join(source, "volume.vtk"), os.path.join(target, "volume.vtk"))
        # shutil.copy(os.path.join(cur_dir, "voxelized.vts"), os.path.join(output_dir, "voxelized.vts"))
        return True
    else:
        return False


def warp_pre_surface_and_hints(cur_dir, displ_dir, initSurface=None, tf=False, output_filename="initSurface_hints_withDispl_deformed.vtu"):
    """Deform preoperative surface and matching cues (hints) by the predicted displacement field from V2S-Net

    Args:
        cur_dir (str): Current folder
        displ_dir (str): Folder where the result of V2S-Net lies.
        initSurface (3d mesh, optional): A 3D mesh which will be warped. Defaults to None.
        tf (bool, optional): Whether to center the surface mesh.
        output_filename (str, optional): Filename of deformed preoperative surface. Defaults to initSurface_hints_withDispl_deformed.vtu.

    Returns:
        bool: Return the flag indicates success or failure
    """
    if initSurface:
        mesh = initSurface
    else:
        if not os.path.exists(os.path.join(cur_dir, "initSurface_hints.vtp")):
            print("initSurface_hints.vtp not exists")
            return False
        else:
            mesh = vtkutils.loadMesh(os.path.join(cur_dir, "initSurface_hints.vtp"))

    if not os.path.exists(os.path.join(displ_dir, "voxelizedResult.vts")):
        print("voxelizedResult.vts not exists")
        return False
    else:
        voxel_grid = vtkutils.loadStructuredGrid(os.path.join(displ_dir, "voxelizedResult.vts"))
    
    print("interpolate displacement field to mesh points...")
    mesh_with_displ = vtkutils.displ_field_interpolate(mesh, voxel_grid, cur_dir, tf_flag=tf, output_filename="initSurface_withDispl.vtu")
    if not mesh_with_displ:
        return False
    print("warping......")
    mesh_warped = vtkutils.warp_mesh(mesh_with_displ, "estimatedDisplacement", cur_dir, output_filename)
    return True



def warp_pre_volume(cur_dir, displ_dir, original_dir):
    '''
        Warp preoperative volume, interpolate displacement error onto all points in the volume for later new ground truth generation

    Args:
        cur_dir (str): Current folder
        displ_dir (str): Folder where the result of V2S-Net lies.
        original_dir (str): Corresponding original V2S dataset folder, used to copy missing files.

    Returns:
        bool: Return the flag indicates success or failure
    '''
    try:
        if os.path.exists(os.path.join(cur_dir, "volume.vtk")):
            init_volume = vtkutils.loadMesh(os.path.join(cur_dir, "volume.vtk"))
        else:
            # copy from original dataset (V2SDataStaticSolverNoRigid) to the cur_dir if the data is missing 
            shutil.copy2(src=os.path.join(original_dir, "volume.vtk"), dst=os.path.join(cur_dir, "volume.vtk"))
            # print("file {} copied to {}".format(src_filepath, dst_filepath))
            init_volume = vtkutils.loadMesh(os.path.join(original_dir, "volume.vtk"))
        print(init_volume.GetNumberOfPoints())

        voxel_grid = vtkutils.loadStructuredGrid(os.path.join(displ_dir, "voxelizedResult.vts"))

        print("interpolate displacement field to volume...")
        volume_with_displ = vtkutils.displ_field_interpolate(init_volume, voxel_grid, cur_dir)
        if not volume_with_displ:
            # print("go next...")
            return False
        print("warping......")
        volume_warped = vtkutils.warp_mesh(volume_with_displ, "estimatedDisplacement", cur_dir, "volume_withDispl_deformed.vtu")
        return True
    except Exception as e:
        traceback.print_exc()
        return False



def warp_arbitrary_mesh(cur_dir, displ_dir=None, mesh=None, voxel_grid=None, filename_mesh=None, filename_voxel_grid=None, tf_flag=True, output_folder="", output_filename_withDispl="", output_filename_deformed=""):
    """
        Warp any meshes

    Args:
        cur_dir (str): Current folder
        displ_dir (str, optional): Folder where the result of V2S-Net lies. Defaults to None.
        mesh (3d mesh, optional): Mesh to be deformed. Defaults to None.
        voxel_grid (vtk voxel grid, optional): Voxel grid that contains the displacement field. Defaults to None.
        filename_mesh (str, optional): Path to the mesh to be deformed. Defaults to None.
        filename_voxel_grid (str, optional): Path to the voxel grid that contains the displacement field. Defaults to None.
        output_folder (str, optional): Folder where to save the deformed mesh. Defaults to None.

    Returns:
        3d mesh: Return the deformed mesh
    """

    try:
        if mesh:
            mesh = mesh
        else:
            if filename_mesh:
                if os.path.exists(os.path.join(cur_dir, filename_mesh)):
                    mesh = vtkutils.loadMesh(os.path.join(cur_dir, filename_mesh))
                else:
                    print("file not exists, please check", os.path.join(cur_dir, filename_mesh))
            else:
                print("please give at least mesh or filename of mesh!")
                return
        print(mesh.GetNumberOfPoints())

        if voxel_grid:
            voxel_grid = voxel_grid
        else:
            if filename_voxel_grid:
                if os.path.exists(os.path.join(displ_dir, filename_voxel_grid)):
                    voxel_grid = vtkutils.loadStructuredGrid(os.path.join(displ_dir, filename_voxel_grid))
                else:
                    print("file not exists, please check", os.path.join(displ_dir, filename_voxel_grid))
            else:
                print("please give at least voxel_grid or filename of voxel_grid!")
                return

        if not output_folder:
            output_folder = cur_dir

        print("interpolate displacement field from voxel grid to mesh...")
        volume_with_displ = vtkutils.displ_field_interpolate(mesh, voxel_grid, output_folder, output_filename=output_filename_withDispl)
        if not volume_with_displ:
            print("interpolation went wrong!")
            return False
        print("warping......")
        volume_warped = vtkutils.warp_mesh(volume_with_displ, "estimatedDisplacement", output_folder, output_filename_deformed)
        return volume_warped
    except Exception as e:
        traceback.print_exc()
        return None



def get_correpsondance_for_intra_surface(cur_dir, pre_surface=None, intra_surface=None):
    """Calculate the correspondece surface patch of intraoperative surface on the deformed preoperative surface by V2S-Net .

    Args:
        cur_dir (str): Current folder
        pre_surface (3d mesh, optional): Deformed preoperative surface. Defaults to None.
        intra_surface (3d mesh, optional): Intraoperative surface. Defaults to None.

    Returns:
        None
    """
    try:
        if pre_surface:
            pre_surface_deformed = pre_surface
        else:
            if not os.path.exists(os.path.join(cur_dir, "volume_withDispl_deformed.vtu")):
                print(utils.bcolors.OKCYAN + "volume_withDispl_deformed.vtu not exists, go next..." + utils.bcolors.ENDC)
                return 
            pre_volume_deformed = vtkutils.loadMesh(os.path.join(cur_dir, "volume_withDispl_deformed.vtu"))
            # print(pre_volume_deformed.GetNumberOfPoints())
            pre_surface_deformed = vtkutils.extractSurface(pre_volume_deformed)
        print(pre_surface_deformed.GetNumberOfPoints())
        if intra_surface:
            intra_surface=intra_surface
        else:
            intra_surface = vtkutils.loadMesh(os.path.join(cur_dir, "partialDeformedSurface.vtp"))

        correspondance_on_deformed_pre_array = vtk.vtkDoubleArray()
        correspondance_on_deformed_pre_array.SetNumberOfComponents(1)
        correspondance_on_deformed_pre_array.SetNumberOfTuples(pre_surface_deformed.GetNumberOfPoints())
        correspondance_on_deformed_pre_array.SetName("correspondance_on_deformed_pre_array")
        if pre_surface_deformed.GetPointData().HasArray("correspondance_on_deformed_pre_array"):
            pre_surface_deformed.GetPointData().RemoveArray("correspondance_on_deformed_pre_array")
        for idx in range(pre_surface_deformed.GetNumberOfPoints()):
            correspondance_on_deformed_pre_array.SetTuple(idx, [0,]) 
        pre_surface_deformed.GetPointData().AddArray(correspondance_on_deformed_pre_array)
        correspondance_on_deformed_pre_array = pre_surface_deformed.GetPointData().GetArray("correspondance_on_deformed_pre_array")

        locator = vtk.vtkPointLocator()
        locator.SetDataSet(pre_surface_deformed)
        locator.Update()

        for i in range(0, intra_surface.GetNumberOfPoints()):
            radius = 0.008
            cell_pts_idx = vtk.vtkIdList()
            # hints_intra = hints_index_array_intra_partial.GetTuple(i)
            cell_pts_valid = False
            while not cell_pts_valid:
                locator.FindPointsWithinRadius( radius, intra_surface.GetPoint(i), cell_pts_idx)
                # print("number of nearest points:", cell_pts_idx.GetNumberOfIds())
                if cell_pts_idx.GetNumberOfIds() == 0:
                    # print(i, "number of nearest points:", cell_pts_idx.GetNumberOfIds())
                    radius += 0.001
                    # print("could not find the closet point(s) for point {}, increase radius to {} and re-generate...".format(i, radius))
                else:
                    cell_pts_valid = True

            for j in range(cell_pts_idx.GetNumberOfIds()):
                # grid_array.SetTuple1(cell_pts_idx.GetId(j), field_value)
                p_id = cell_pts_idx.GetId(j)
                # print(p_id)
                correspondance_on_deformed_pre_array.SetTuple(p_id, [1,]) 

        thresh = vtk.vtkThresholdPoints()
        thresh.SetInputData( pre_surface_deformed )
        thresh.SetInputArrayToProcess(0, 0, vtk.vtkDataObject.FIELD_ASSOCIATION_POINTS, vtk.vtkDataSetAttributes.SCALARS, "correspondance_on_deformed_pre_array" )
        thresh.ThresholdByUpper(0.9)
        thresh.Update()

        correspondance_on_deformed_pre = thresh.GetOutput()
        print(correspondance_on_deformed_pre.GetNumberOfPoints())
        correspondance_on_deformed_pre = vtkutils.unstructuredGridToPolyData( thresh.GetOutput() )

        vtkutils.writeMesh(correspondance_on_deformed_pre, os.path.join(cur_dir, "correspondance_on_deformed_pre.vtp"))

    except Exception as e:
        traceback.print_exc()




def voxelization_warped_pre_surface(cur_dir, initSurface=None, voxel_grid=None, tf=None, original_dir=None, grid_to_cur=False):
    """Voxelize the deformed preoperative surface.

    Args:
        cur_dir (str): Current folder
        initSurface (3d mesh, optional): Deformed preoperative surface. Defaults to None.
        voxel_grid (vtk voxel grid, optional): Voxel grid. Defaults to None.
        tf (vtk transform, optional): Transformation matrix. Defaults to None.
        original_dir (str, optional): Original V2S dataset folder, used to copy missing files. Defaults to None.
        grid_to_cur (bool, optional): Whether to save the resulting voxel grid to current folder.

    Returns:
        vtk voxel grid: Voxel grid which stores the voxelized deformed preoperative surface.
    """
    try:
        print("only deformed pre surface to DF------------")
        if not initSurface:
            if os.path.exists(os.path.join(cur_dir, "initSurface_hints_withDispl_deformed.vtu")):
                initSurface = vtkutils.loadMesh(os.path.join(cur_dir, "initSurface_hints_withDispl_deformed.vtu"))
                initSurface = vtkutils.unstructuredGridToPolyData(initSurface)
                print("removing double faces...")
                print("before removing:", initSurface.GetNumberOfCells())
            elif os.path.exists(os.path.join(cur_dir, "volume_withDispl_deformed.vtu")):
                volume_withDispl_deformed = vtkutils.loadMesh(os.path.join(cur_dir, "volume_withDispl_deformed.vtu"))
                initSurface = vtkutils.unstructuredGridToPolyData(volume_withDispl_deformed)
                initSurface = vtkutils.extractSurface(initSurface)
                print("removing double faces...")
                print("before removing:", initSurface.GetNumberOfCells())
                # code updated...no double faces problem
            initSurface_reduced_faces = vtkutils.removeDoubleFaces(initSurface)
            print("after removing:", initSurface_reduced_faces.GetNumberOfCells())
            initSurface = initSurface_reduced_faces

        if type(isinstance(initSurface, vtk.vtkUnstructuredGrid)):
            print("convert vtkUnstructuredGrid to vtkPolyData")
            initSurface = vtkutils.unstructuredGridToPolyData(initSurface)

        if voxel_grid:
            voxel_grid = voxel_grid
        else:
            if not os.path.exists(os.path.join(cur_dir, "voxelized.vts")):
                # load from original dataset (V2SDataStaticSolverNoRigid) if the voxel grid is missing 
                voxel_grid = vtkutils.loadStructuredGrid(os.path.join(original_dir, "voxelized.vts"))
            else:
                voxel_grid = vtkutils.loadStructuredGrid(os.path.join(cur_dir, "voxelized.vts"))
        if tf:
            tf = tf
        else:
            tf = vtkutils.loadTransformationMatrix(grid=voxel_grid)
        
        # warped preoperative surface to DF:
        tfFilter = vtk.vtkTransformFilter()
        tfFilter.SetTransform(tf)
        tfFilter.SetInputData(initSurface)
        tfFilter.Update()
        initSurface = tfFilter.GetOutput()

        voxelize.distanceFieldGPU( initSurface, voxel_grid, "preoperativeSurface" )
        voxelize.to_SDF( initSurface, voxel_grid, "preoperativeSurface" )

        if grid_to_cur:
            writer = vtk.vtkXMLStructuredGridWriter()
            writer.SetFileName(os.path.join(cur_dir, "voxelized.vts"))
            writer.SetInputData(voxel_grid)
            writer.Update()
            print("File:{} saved".format(os.path.join(cur_dir, "voxelized.vts")))

        return voxel_grid
    except Exception as e:
        traceback.print_exc()
        return None


def voxelization_warped_surface_hints(cur_dir, original_dir, voxel_grid_dir=None, initSurface=None, voxel_grid=None, ):
    """Voxelize the deformed preoperative surface and matching cues (displacement hints).

    Args:
        cur_dir (str): Current folder
        original_dir (str): Original V2S dataset folder, used to copy missing files.
        voxel_grid_dir (str, optional):Folder where the voxel grid file lies. Defaults to None.
        initSurface (3d mesh, optional): Deformed preoperative surface. Defaults to None.
        voxel_grid (vtk voxel grid, optional): Voxel grid. Defaults to None.

    Returns:
        bool: Success or failure.
    """
    try:
        print("hints index array to DF:------------")
        if not initSurface:
            initSurface = vtkutils.loadMesh(os.path.join(cur_dir, "initSurface_hints_withDispl_deformed.vtu"))
        initSurface = vtkutils.unstructuredGridToPolyData(initSurface)
        print("removing double faces...")
        print("cells number before removing:", initSurface.GetNumberOfCells())

        initSurface_reduced_faces = vtkutils.removeDoubleFaces(initSurface)
        print("cells number after removing:", initSurface_reduced_faces.GetNumberOfCells())
        # initSurface_without_hints = vtkutils.extractSurface(initSurface)
        # triangele_filter = vtk.vtkTriangleFilter()
        # triangele_filter.SetInputData(initSurface_without_hints)
        # triangele_filter.Update()
        # initSurface_without_hints = triangele_filter.GetOutput()

        # vtkutils.writeMesh(initSurface_without_hints, os.path.join(cur_dir, "test_surface.stl"))
        # initSurface_without_hints = vtkutils.loadMesh(os.path.join(cur_dir, "initSurface_hints.vtp"))

        # featureEdges = vtk.vtkFeatureEdges()
        # featureEdges.SetInputData(initSurface_without_hints)
        # featureEdges.BoundaryEdgesOn()
        # featureEdges.FeatureEdgesOff()
        # featureEdges.ManifoldEdgesOff()
        # featureEdges.NonManifoldEdgesOff()
        # featureEdges.Update()
        # numberOfOpenEdges = featureEdges.GetOutput().GetNumberOfCells()
        # print("number of open edges:", numberOfOpenEdges)
            # vtkutils.writeMesh(mesh=initSurface, filename=os.path.join(cur_dir, "initSurface_hints_withDispl_deformed.vtp"))
        # initSurface_without_hints = vtkutils.loadMesh(os.path.join(cur_dir, "surface.stl"))

        # voxel_grid = vtkutils.loadStructuredGrid(os.path.join(displ_dir, "voxelizedResult.vts"))
        if voxel_grid:
            voxel_grid = voxel_grid
        else:
            if not voxel_grid_dir:
                voxel_grid = vtkutils.loadStructuredGrid(os.path.join(original_dir, "voxelized.vts"))
            else:
                if not os.path.exists(os.path.join(voxel_grid_dir, "voxelized.vts")):
                    voxel_grid = vtkutils.loadStructuredGrid(os.path.join(original_dir, "voxelized.vts"))
                else:
                    voxel_grid = vtkutils.loadStructuredGrid(os.path.join(voxel_grid_dir, "voxelized.vts"))
        hints_index_array_pre = initSurface.GetPointData().GetArray("hints_index_array")

   
        hints_index_array_pre_single = vtk.vtkFloatArray()
        hints_index_array_pre_single.SetNumberOfComponents(1)  # length of a tuple
        hints_index_array_pre_single.SetNumberOfTuples(initSurface.GetNumberOfPoints())  # length of this array (how many tuples)
        hints_index_array_pre_single.SetName("hints_index_array_pre_single")
        initSurface.GetPointData().AddArray(hints_index_array_pre_single)
        
        tf = vtkutils.loadTransformationMatrix(grid=voxel_grid)
        
        # warped preoperative surface to DF:
        tfFilter = vtk.vtkTransformFilter()
        tfFilter.SetTransform(tf)
        tfFilter.SetInputData(initSurface_reduced_faces)
        tfFilter.Update()
        initSurface_reduced_faces = tfFilter.GetOutput()


        voxelize.distanceFieldGPU( initSurface_reduced_faces, voxel_grid, "preoperativeSurface" )
        voxelize.to_SDF( initSurface_reduced_faces, voxel_grid, "preoperativeSurface" )
        print("preoperative surface voxelized")
        # warped preoperative displacement hints to DF:
        for i in range(0, 4):
            print("hints pre:")
            c_pre = 0
            for id in range(0, initSurface.GetNumberOfPoints()):
                hints_index_tuple_pre = hints_index_array_pre.GetTuple(id)
                
                if hints_index_tuple_pre[i] == 1:
                    c_pre += 1
                hints_index_array_pre_single.SetTuple1(id, hints_index_tuple_pre[i])

            thresh = vtk.vtkThreshold()
            thresh.SetInputData( initSurface )
            thresh.SetInputArrayToProcess( 0,0,0, vtk.vtkDataObject.FIELD_ASSOCIATION_POINTS, "hints_index_array_pre_single" )
            thresh.ThresholdBetween( 0.9, 1 )
            # thresh.ThresholdByLower(0.9)
            thresh.Update()

            partialSurface = vtkutils.unstructuredGridToPolyData( thresh.GetOutput() )
            # print(partialSurface.GetPointData().HasArray("hints_index_array_single"))
            # hints_index_array_single_partial = partialSurface.GetPointData().GetArray("hints_index_array_single")
            # for j in range(0, partialSurface.GetNumberOfPoints()):
            #     print(hints_index_array_single_partial.GetTuple1(j))
            print("{} number of points:".format(i), partialSurface.GetNumberOfPoints())
            print("{} hints larger than 1".format(i), c_pre)

            enable_debug = True
            if enable_debug:
                if os.path.exists(os.path.join(cur_dir, "hints_pre_test_{}.vtp".format(i))):
                    os.remove(os.path.join(cur_dir, "hints_pre_test_{}.vtp".format(i)))
                writer = vtk.vtkXMLPolyDataWriter()
                writer.SetFileName(os.path.join(cur_dir, "hints_pre_test_{}.vtp".format(i)))
                writer.SetInputData(partialSurface)
                writer.Update()

            tfFilter = vtk.vtkTransformFilter()
            tfFilter.SetTransform(tf)
            tfFilter.SetInputData(partialSurface)
            tfFilter.Update()
            partialSurface = tfFilter.GetOutput()

            if not partialSurface:
                print("{}th partial surface is None".format(i)) 
                return False

            # voxelize.distanceField(surfaceMesh=partialSurface, targetGrid=voxel_grid, targetArrayName="hints_index_pre_DF_{}".format(i))
            if voxel_grid.GetPointData().HasArray("hints_index_pre_DF_{}".format(i)):
                voxel_grid.GetPointData().RemoveArray("hints_index_pre_DF_{}".format(i))

            voxelize.distanceFieldGPU(surface=partialSurface, targetGrid=voxel_grid, targetArrayName="hints_index_pre_DF_{}".format(i))

        writer = vtk.vtkXMLStructuredGridWriter()
        writer.SetFileName(os.path.join(cur_dir, "voxelized.vts"))
        writer.SetInputData(voxel_grid)
        writer.Update()
        print("File:{} saved".format(os.path.join(cur_dir, "voxelized.vts")))

        return True
    except Exception as e:
        traceback.print_exc()
        return False



def voxelization_displ_err_volume(cur_dir, original_dir, init_volume=None, voxel_grid=None):
    """Voxlize deformed preoperative volume and displacement error.

    Args:
        cur_dir (str): Current folder
        original_dir (str): Original V2S dataset folder, used to copy missing files.
        init_volume (3d mesh, optional): Deformed preoperative volume.
        voxel_grid (vtk voxel grid, optional): Voxel grid. Defaults to None.

    Returns:
        bool: Success or failure.
    """
    try:
        print("interpolate warped displacement error to voxel grid------------")
        if not init_volume:
            init_volume = vtkutils.loadMesh(os.path.join(cur_dir, "volume_withDispl_deformed.vtu"))
            init_volume = vtkutils.unstructuredGridToPolyData(init_volume)
            # print(init_volume.GetPointData().HasArray("displacementError"))
        else:
            init_volume = init_volume
        
        if not voxel_grid:
            # if not os.path.exists(os.path.join(cur_dir, "voxelized.vts")) and os.path.exists(os.path.join(original_dir, "voxelized.vts")):
            #     print("there maybe some copy error before, redo the voxelization of warped pre surface and hints...")
            
            voxel_grid = vtkutils.loadStructuredGrid(os.path.join(cur_dir, "voxelized.vts"))
        else:
            voxel_grid = voxel_grid

        tf = vtkutils.loadTransformationMatrix(grid=voxel_grid)
        
        # warped preoperative surface to DF:
        tfFilter = vtk.vtkTransformFilter()
        tfFilter.SetTransform(tf)
        tfFilter.SetInputData(init_volume)
        tfFilter.Update()
        init_volume = tfFilter.GetOutput()
        

        # interpolation using GPU:
        # displ_error_interpolated = voxelize_displacement.interpolateToGridGPU(mesh=init_volume, grid=voxel_grid, cellSize=0.3 / 63, radius=3, array_name="displacementError")
        # print("GPU:", torch.mean(displ_error_interpolated))
        # displ_error_vtk = numpy_to_vtk(displ_error_interpolated.cpu().detach())
        # displ_error_vtk.SetName("displacement_GPU")
        # voxel_grid.GetPointData().AddArray(displ_error_vtk)
        # # print(type(displ_error_vtk))
        # print(grid_interpolated.shape)
        
        # interpolation using VTK:
        voxel_grid_interpolated = voxelize_displacement.interpolateToGrid(mesh=init_volume, grid=voxel_grid, cellSize=0.3/63, radius_num=3, sharpness=0)
        # voxel_grid_interpolated = voxelize_displacement.interpolateToGridGPU(mesh=init_volume, grid=voxel_grid, cellSize=0.3/63, radius=3, array_name="displacementError")
        # print(type(voxel_grid_interpolated))
        displ_err = voxel_grid_interpolated.GetPointData().GetArray("displacementError")
        # displ_err.SetName("displacementError_new_gt")
        displ_err.SetName("displacement_residual")
        if voxel_grid.GetPointData().HasArray("displacement_residual"):
            voxel_grid.GetPointData().RemoveArray("displacement_residual")
        voxel_grid.GetPointData().AddArray(displ_err)

        displ_err_numpy = vtk_to_numpy(displ_err)
        print("VTK:", torch.mean(torch.FloatTensor(displ_err_numpy)))
        

        writer = vtk.vtkXMLStructuredGridWriter()
        writer.SetFileName(os.path.join(cur_dir, "voxelized.vts"))
        # writer.SetFileName(os.path.join(cur_dir, "voxelized_test_displ_error_volume.vts"))
        writer.SetInputData(voxel_grid)
        writer.Update()
        print("File:{} saved".format(os.path.join(cur_dir, "voxelized.vts")))

        return True
    except Exception as e:
        traceback.print_exc()
        if os.path.exists(os.path.join(cur_dir, "voxelized.vts")):
            print("something wrong, rename from voxelized.vst to voxelized_wrong.vts")
            os.rename(src=os.path.join(cur_dir, "voxelized.vts"), dst=os.path.join(cur_dir, "voxelized_wrong.vts"))
        return False



def voxelize_arbitrary_mesh(cur_dir, mesh=None, mesh_filename=None, voxel_grid=None, voxel_grid_filename="voxelized.vts", array_name=None, sdf=False, tf_flag=True, output_filename="voxelized.vts"):
    """Function which calculates the SDF or DF for a mesh, stores the calculated information as an array of the voxel grid

    Args:
        cur_dir (str): Current folder
        mesh (3d mesh, optional): A 3D mesh which will be voxelized. Defaults to None.
        mesh_filename (str, optional): Path to the 3D mesh, when it is not provided directly. Defaults to None.
        voxel_grid (vtk voxel grid, optional): voxel grid carries all the information. Defaults to None.
        voxel_grid_filename (str, optional): Path to the voxel grid, when it is not provided directly. Defaults to "voxelized.vts".
        array_name (str, optional): Array name of the SDF or DF of the mesh showed in the voxel grid. Defaults to None.
        sdf (bool, optional): SDF or DF. Defaults to False.
        tf_flag (bool, optional): Apply transformation or not. Defaults to True.
        output_filename (str, optional): Output path of the voxel grid. Defaults to "voxelized.vts".

    Returns:
        vtk voxel grid: Return the voxel grid which contains the calculated SDF or DF
    """
    try:
        if mesh:
            mesh = mesh
        else:
            if not mesh_filename:
                print(utils.bcolors.OKCYAN + "please provide mesh or filename of mesh" + utils.bcolors.ENDC)
                return
            else:
                if not os.path.exists(os.path.join(cur_dir, mesh_filename)):
                    print(utils.bcolors.OKCYAN + "file {} not exists".format(mesh_filename) + utils.bcolors.ENDC)
                    return
                else:
                    mesh = vtkutils.loadMesh(os.path.join(cur_dir, mesh_filename))
        print("number of points of mesh to be voxelized:", mesh.GetNumberOfPoints())
        if voxel_grid:
            voxel_grid = voxel_grid
        else:
            if not voxel_grid_filename:
                print("please provide voxel_grid or filename of voxel_grid")
                return
            else:   
                if not os.path.exists(os.path.join(cur_dir, voxel_grid_filename)):
                    print(utils.bcolors.OKCYAN + "file {} not exists".format(voxel_grid_filename) + utils.bcolors.ENDC)
                    return
                else:
                    voxel_grid = vtkutils.loadStructuredGrid(os.path.join(cur_dir, voxel_grid_filename))
        
        if tf_flag:
            tf = vtkutils.loadTransformationMatrix(grid=voxel_grid)
            tfFilter = vtk.vtkTransformFilter()
            tfFilter.SetTransform(tf)
            tfFilter.SetInputData(mesh)
            tfFilter.Update()
            mesh = tfFilter.GetOutput()

        if not array_name :
            array_name = os.path.splitext(mesh_filename)

        if voxel_grid.GetPointData().HasArray(array_name):
            voxel_grid.GetPointData().RemoveArray(array_name)

        voxelize.distanceFieldGPU( mesh, voxel_grid, array_name )
        if sdf:
            voxelize.to_SDF( mesh, voxel_grid, array_name )

        if output_filename:
            print("writing to disk...")
            writer = vtk.vtkXMLStructuredGridWriter()
            writer.SetFileName(os.path.join(cur_dir, output_filename))
            writer.SetInputData(voxel_grid)
            writer.Update()
            print("File:{} saved".format(os.path.join(cur_dir, output_filename)))

        return voxel_grid

    except Exception as e:
        traceback.print_exc()


def copy_estimatedDispl(cur_dir, displ_dir, force=False):
    voxel_grid = vtkutils.loadStructuredGrid(os.path.join(cur_dir, "voxelized.vts"))
    if voxel_grid.GetPointData().HasArray("estimatedDisplacement") and force == False:
        print("esitmatedDisplacement already exists")
        return True
    else:
        voxel_grid_dipl = vtkutils.loadStructuredGrid(os.path.join(displ_dir, "voxelizedResult.vts"))

        estimated_displ = voxel_grid_dipl.GetPointData().GetArray("estimatedDisplacement")
        voxel_grid.GetPointData().AddArray(estimated_displ)

        vtkutils.writeMesh(voxel_grid, os.path.join(cur_dir, "voxelized.vts"))
        print("saved", os.path.join(cur_dir, "voxelized.vts"))



if __name__ == '__main__':
    pointer = 0
    invalid_list = []
    parser = argparse.ArgumentParser(description="Deform preoperative surface and displacement hints as the input for Cue-Net.")
    parser.add_argument("--datapath", type=str, required=True, help="Path to the training set of Cue-Net")
    parser.add_argument("--displ_folder", required=True, type=str, help="Folder where the .vts files lie, which contain estimatedDisplacement")
    parser.add_argument("--num", type=int, default=1000, help="Amount of data samples to load")
    parser.add_argument("--start", type=int, default=0, help="Start number of folder")
    parser.add_argument("--copy_when_not_exists", action="store_true", help="create folder when missing in the cur_dir, often used for a new datapath")
    # parser.add_argument("--outputFolder", required=True, type=str, help="Folder in which to save the results and models")
    args = parser.parse_args()

    data_path = args.datapath
    num = args.num
    start = args.start
    displ_folder = args.displ_folder
    copy_when_not_exists = args.copy_when_not_exists

    # path to V2SNet dataset
    original_folder = "."

    try:
        print(data_path, num)
        if not os.path.exists(data_path):
            raise ValueError("Path {} not exists".format(data_path))

        end = start + num
        
        for p in range(start, end):
            original_dir = os.path.join(original_folder, "{:06d}".format(p))
            cur_dir = os.path.join(data_path, "{:06d}".format(p))
            if not os.path.exists(cur_dir):
                if copy_when_not_exists:
                    if os.path.exists(os.path.join(original_dir, "voxelized.vts")):
                        os.makedirs(cur_dir)
                        copy_files(source=original_dir, target=cur_dir)
                    else:
                        print("Even original data is not complete, go next...")
                        continue
                else:
                    print("path:{} not exists, task may be done".format(cur_dir))
                    continue

            displ_dir = os.path.join(displ_folder, "{:06d}".format(p))

            print("====================Dealing with directory:{}========================".format(cur_dir))
            # copy files:
            # if copy_files(cur_dir, output_dir):
            #     print("copy files finished")
            # else:
            #     print("nothing to copy")

            # warp preoperative surface and hints:
            # initSurface = vtkutils.loadMesh(os.path.join(cur_dir, "surface.stl"))
            # if warp_pre_surface_and_hints(cur_dir, displ_dir, tf=True, initSurface=initSurface, output_filename="initSurface_res_deformed.vtu"):
            #     print("warp preoperative surface and hints success")
            # else:
            #     print(utils.bcolors.OKCYAN + "warp sample {} failed, go next...".format(cur_dir) + utils.bcolors.ENDC)
            #     continue

            # warp preoperative surface
            initSurface = vtkutils.loadMesh(os.path.join(cur_dir, "surface.stl"))
            if warp_arbitrary_mesh(cur_dir, displ_dir=displ_dir, mesh=initSurface, filename_voxel_grid="voxelizedResult.vts", tf_flag=True, output_folder=cur_dir, output_filename_deformed="initSurface_res_deformed.vtu"):
                print("warp preoperative surface and hints success")
            else:
                print(utils.bcolors.OKCYAN + "warp sample {} failed, go next...".format(cur_dir) + utils.bcolors.ENDC)
                continue

            # warp preoperative volume
            if warp_pre_volume(cur_dir=cur_dir, displ_dir=displ_dir, original_dir=original_dir):
                print("warp preoperative volume success")
            else:
                print(utils.bcolors.OKCYAN + "warp sample {} failed, go next...".format(cur_dir) + utils.bcolors.ENDC)
                break

            # get corresponding preoperative surface path for intraoperative surface and voxelize it
            get_correpsondance_for_intra_surface(cur_dir=cur_dir,)
            voxelize_arbitrary_mesh(cur_dir=cur_dir, mesh_filename="correspondance_on_deformed_pre.vtp", array_name="correspondance_of_intra_on_deformed_pre", )


            # voxelize deformed preoperative surface and matching cues
            # if voxelization_warped_surface_hints(cur_dir=cur_dir, voxel_grid_dir=cur_dir, original_dir=original_dir):
            #     print("voxelization success")
            # else:
            #     print(utils.bcolors.OKCYAN + "voxelization failed, go next..." + utils.bcolors.ENDC)

            # voxelize deformed preoperative surface ONLY:
            pre_surface = vtkutils.loadMesh(os.path.join(cur_dir, "initSurface_res_deformed.vtu"))
            voxelization_warped_pre_surface(cur_dir=cur_dir, initSurface=pre_surface, original_dir=original_dir, grid_to_cur=True)

            # interpolate displacement error from warped init volume to voxel grid
            voxelization_displ_err_volume(cur_dir=cur_dir, original_dir=original_dir)
    except Exception as e:
        traceback.print_exc()

