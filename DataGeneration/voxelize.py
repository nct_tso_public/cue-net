from vtk import *
import math
import inspect
import sys
import os

# sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),"../Utils"))
# from generalutils import *
from vtkutils import *
from pytorchutils import *



def distanceFieldGPU(surface, targetGrid, targetArrayName):
    device = on_gpu()

    surface_tensor = torch.from_numpy(vtkPointSetToNumpyArray(surface)).float().to(device)
    grid_tensor = torch.from_numpy(vtkPointSetToNumpyArray(targetGrid)).float().to(device)

    batch_size_estimate = estimate_grid_mesh_batch_size(grid_tensor, surface_tensor, device=device)
    df_tensor = Distances.batched_min_cdist(grid_tensor, surface_tensor, batch_size_estimate)

    df = numpyArrayToVTK(df_tensor.cpu())
    df.SetName(targetArrayName)
    targetGrid.GetPointData().AddArray(df)

    return df


def to_SDF(surfaceMesh, targetGrid, targetArrayName):
    # surfaceMesh.GetCell(0).GetPointIds().GetId(0) for corner of first triangle
    pts = vtkPolyData()
    pts.SetPoints(targetGrid.GetPoints())

    enclosedPointSelector = vtkSelectEnclosedPoints()
    e = ErrorObserver()
    enclosedPointSelector.AddObserver("ErrorEvent", e)
    enclosedPointSelector.GetExecutive().AddObserver("ErrorEvent", e)

    enclosedPointSelector.CheckSurfaceOn()
    enclosedPointSelector.SetInputData(pts)
    enclosedPointSelector.SetSurfaceData(surfaceMesh)

    enclosedPointSelector.SetTolerance(1e-9)
    enclosedPointSelector.Update()

    if e.ErrorOccurred():
        raise Exception( "Could not calculate enclosed points. Maybe mesh is not closed?\nFull error was: " + e.ErrorMessage())
    sdf = targetGrid.GetPointData().GetArray(targetArrayName)

    for i in range(0, targetGrid.GetNumberOfPoints()):
        if enclosedPointSelector.IsInside(i):
            sdf.SetTuple1(i, -sdf.GetTuple1(i))  # invert sign

    return sdf

