import torch
import torch.nn as nn
import data
import os
import statistics
import traceback
import random
from loss import Loss, HintsSurfaceLoss


def testModelBatched( model, dataloader, printErrors=True ):

    stats = []

    with torch.no_grad():
        model.eval()

        for i_batch, d in enumerate(dataloader):

            try:

                preoperative = d["preoperative"].cuda( non_blocking=True )
                intraoperative = d["intraoperative"].cuda( non_blocking=True )
                intraoperative_corresp_on_pre = d["intraoperative_corresp_on_pre"].cuda(non_blocking=True)
                hintsindexarraypre = d["hintsindexarraypre"].cuda(non_blocking=True)
                hintsindexarrayintra = d['hintsindexarrayintra'].cuda(non_blocking=True)
                out64, out32, out16, out8 = model( preoperative, intraoperative, intraoperative_corresp_on_pre, hintsindexarraypre, hintsindexarrayintra )
                mask64 = (preoperative <= 0).float()

                # Prepare target data:
                target64 = d["targetDisplacement"].cuda( non_blocking=True )
                target32 = nn.functional.interpolate( target64, size=32 )
                target16 = nn.functional.interpolate( target64, size=16 )
                target8 = nn.functional.interpolate( target64, size=8 )
                preop32 = nn.functional.interpolate( preoperative, size=32 )
                preop16 = nn.functional.interpolate( preoperative, size=16 )
                preop8 = nn.functional.interpolate( preoperative, size=8 )

                # Loss calculation:
                l64 = Loss( out64, target64, torch.abs(preoperative) )
                l32 = Loss( out32, target32, torch.abs(preop32) )
                l16 = Loss( out16, target16, torch.abs(preop16) )
                l8 = Loss( out8, target8, torch.abs(preop8) )

                loss = 10*l64 + l32 + l16 + l8 

                batchSize = preoperative.shape[0]

                errDisplacement = ((target64 - out64))
                displacementErrMagnitude = torch.norm( errDisplacement, dim=1, keepdim=True )
                displacementErrMagnitudeMasked = displacementErrMagnitude*mask64
                meanDisplacementErr = torch.sum( displacementErrMagnitudeMasked )/torch.sum( mask64 )


                s = { "loss": loss.item(),
                        "meanDisplacementErr": meanDisplacementErr.item(),
                        }
                stats.append(s)
                    
                del errDisplacement
                del meanDisplacementErr, displacementErrMagnitude
                del displacementErrMagnitudeMasked

                del preoperative, intraoperative, hintsindexarrayintra, hintsindexarraypre, intraoperative_corresp_on_pre
                del mask64
                del target64, target32, target16, target8, 
                del out64, out32, out16, out8, 
                del l64, l32, l16, l8, 
                del loss
                del d

            except KeyboardInterrupt as err:    # If a keyboard interrupt was caught, pass it on
                print("KeyboardInterrupt")
                raise( err )
            except Exception as err:
                if printErrors:
                    print("\n",traceback.format_exc())
                    print(err)

    return stats

def testModel( model, dataset, verbose=False, saveResults=False, outputFolder="", printErrors=True, testTiming=False, shuffle_flag=True, hintsNum=4 ):
    stats = []

    with torch.no_grad():
        model.eval()

        for i in range( 0, len(dataset) ):

            try:
                if testTiming:
                    torch.cuda.synchronize()  # Wait for the events to be recorded!
                    start_event = torch.cuda.Event(enable_timing=True)
                    end_event = torch.cuda.Event(enable_timing=True)
                    start_event.record()

                d = dataset[i]

                preoperative = d["preoperative"].cuda( non_blocking=True ).unsqueeze(0)
                intraoperative = d["intraoperative"].cuda( non_blocking=True ).unsqueeze(0)
                intraoperative_corresp_on_pre = d["intraoperative_corresp_on_pre"].cuda(non_blocking=True).unsqueeze(0)
                hintsindexarraypre = d["hintsindexarraypre"].cuda(non_blocking=True).unsqueeze(0)
                hintsindexarrayintra = d['hintsindexarrayintra'].cuda(non_blocking=True).unsqueeze(0)
                
                # hints_mask = [1] * hintsNum + [0] * (4 - hintsNum)
                # if shuffle_flag:
                #     random.shuffle(hints_mask)
                # print("hints mask", hints_mask)
                # hints_mask_zero_idx = []
                # for hint_idx, hint_flag in enumerate(hints_mask):
                #     if hint_flag == 0:
                #         hints_mask_zero_idx.append(hint_idx)
                # print(hints_mask_zero_idx)
                # hintsindexarraypre[:, hints_mask_zero_idx, :] = 0
                # hintsindexarrayintra[:, hints_mask_zero_idx, :] = 0

                # Run model:
                out64, out32, out16, out8 = model(preoperative, intraoperative, intraoperative_corresp_on_pre, hintsindexarraypre, hintsindexarrayintra )
                estimatedDisplacement = (out64).squeeze()
                mask64 = (preoperative <= 0).float()

                if testTiming:
                    end_event.record()
                    torch.cuda.synchronize()  # Wait for the events to be recorded!
                    elapsed_time_ms = start_event.elapsed_time(end_event)

                meanDisplacement = torch.sum( torch.norm( out64*mask64, dim=1 ) )/torch.sum( mask64 )
                maxDisplacement = torch.max( torch.norm( out64*mask64, dim=1 ) )
                #out64.zero_()
                #out32.zero_()
                #out16.zero_()
                #out8.zero_()
                s = { "index":d["index"],
                        "visibleAmount":d["visibleAmount"],
                        "meanDisplacement":meanDisplacement.item(),
                        "maxDisplacement":maxDisplacement.item() }
                print(s)

                if testTiming:
                    s["time"] = elapsed_time_ms

                stats.append( s )

                errDisplacement = None
                if "targetDisplacement" in d:
                    # Set up loss mask:
                    #mask32 = nn.functional.interpolate( mask64, size=32 )
                    #mask16 = nn.functional.interpolate( mask64, size=16 )
                    #mask8 = nn.functional.interpolate( mask64, size=8 )
                    # Prepare target data:
                    target64 = d["targetDisplacement"].cuda( non_blocking=True ).unsqueeze(0)
                    target32 = nn.functional.interpolate( target64, size=32 )
                    target16 = nn.functional.interpolate( target64, size=16 )
                    target8 = nn.functional.interpolate( target64, size=8 )
                    preop32 = nn.functional.interpolate( preoperative, size=32 )
                    preop16 = nn.functional.interpolate( preoperative, size=16 )
                    preop8 = nn.functional.interpolate( preoperative, size=8 )

                    # Loss calculation:
                    l64 = Loss( out64, target64, torch.abs(preoperative) )
                    l32 = Loss( out32, target32, torch.abs(preop32) )
                    l16 = Loss( out16, target16, torch.abs(preop16) )
                    l8 = Loss( out8, target8, torch.abs(preop8) )

                    loss = 10*l64 + l32 + l16 + l8 

                    if verbose:
                        print("{}:\tloss: {:.2e}\tl64: {:.2e}\tl32: {:.2e}\tl16: {:.2e}\tl8: {:.2e}\tlS: {:.2e}".format(d["index"],
                            loss.item(), l64.item(), l32.item(), l16.item(), l8.item(), 0 ) )


                    targetDisplacementMagnitude = torch.norm( target64, dim=1 )
                    meanTargetDisplacement = torch.sum( targetDisplacementMagnitude*mask64 )/torch.sum( mask64 )
                    maxTargetDisplacement = torch.max( targetDisplacementMagnitude )

                    errDisplacement = ((target64 - out64)).squeeze()
                    displacementErrMagnitude = torch.norm( errDisplacement, dim=0 )
                    displacementErrMagnitudeMasked = displacementErrMagnitude*mask64
                    maxDisplacementErr = torch.max( displacementErrMagnitudeMasked )
                    meanDisplacementErr = torch.sum( displacementErrMagnitudeMasked )/torch.sum( mask64 )

                    s["loss"] = loss.item()
                    s["meanDisplacementErr"] = meanDisplacementErr.item()
                    s["maxDisplacementErr"] = maxDisplacementErr.item()
                    s["meanTargetDisplacement"] = meanTargetDisplacement.item()
                    s["maxTargetDisplacement"] = maxTargetDisplacement.item()

                    del maxDisplacementErr, meanDisplacementErr, displacementErrMagnitude
                    del meanDisplacement, displacementErrMagnitudeMasked
                    del loss, l64, l32, l16, l8, target64, target32, target16, target8#, lS

                if verbose and testTiming:
                    print("\tTime: {:f} ms".format(elapsed_time_ms))

                if saveResults:
                    
                    inFile = d["path"]
                    # print(inFile)
                    # print(d["index"])
                    # outFile = os.path.join( outputFolder, "{:05d}".format(d["index"]), "voxelizedResult.vts" )
                    outFile = os.path.join( outputFolder, "{:06d}".format(d["index"]), "voxelizedResult.vts" )
                    resultArrays = {}
                    resultArrays["estimatedDisplacement"] = estimatedDisplacement
                    if errDisplacement is not None:    # If a target is known, also calculate the error:
                        resultArrays["displacementError"] = errDisplacement

                    # Re-load the input grid and save the new arrays with it:
                    data.saveSample( inFile, outFile, resultArrays )
                    if verbose:
                        print("Saved sample to " + outFile)

                del out64, out32, out16, out8,
                if errDisplacement is not None:
                    del errDisplacement


            except KeyboardInterrupt as err:    # If a keyboard interrupt was caught, pass it on
                print("KeyboardInterrupt")
                raise( err )
            except Exception as err:
                if printErrors:
                    print("\n",traceback.format_exc())
                    print(err)

    return stats

