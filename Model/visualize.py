import numpy
import statistics
from utils import *
import matplotlib.pyplot as plt

def calcStats( stats ):
    losses = [d["loss"] for d in stats]
    visAmounts = [d["visibleAmount"] for d in stats]
    meanDisplacementErrs = [d["meanDisplacementErr"] for d in stats]
    meanTargetDisplacements = [d["meanTargetDisplacement"] for d in stats]
    meanDisplacements = [d["meanDisplacement"] for d in stats]
    times = [d["time"] for d in stats]
    if len(times) > 0:
      medianTime = statistics.median(times)
    else:
      medianTime = -1

    print("Mean loss:", sum(losses)/len(losses))
    print("Mean target displacement:", sum(meanTargetDisplacements)/len(meanTargetDisplacements))
    print("Mean displacement error:", sum(meanDisplacementErrs)/len(meanDisplacementErrs) )
    print("Median mean displacement error:", statistics.median(meanDisplacementErrs) )
    print("Median time: {:.2f} (~ {:.2f} FPS)".format( medianTime, 1e3/medianTime ) )


    ret = {
        "mean_loss": sum(losses)/len(losses),
        "mean_target_displacement": sum(meanTargetDisplacements)/len(meanTargetDisplacements),
        "mean_displacement_error": sum(meanDisplacementErrs)/len(meanDisplacementErrs) ,
        "median_displacement_error": statistics.median(meanDisplacementErrs) ,
    }

    # Find samples with largest error in displacement:
    sortedStats = sorted(stats, key=lambda i: i["meanDisplacementErr"])
    worst = sortedStats[-10:]
    print("Samples with worst mean error:")
    for i in range(len(worst)-1,-1,-1):
        w = worst[i]
        print( "\t{}: {:.2e}".format(w["index"], w["meanDisplacementErr"] ) )

    # Find samples with smallest visible amount:
    sortedStats = sorted(stats, key=lambda i: -i["visibleAmount"])
    lowest = sortedStats[-10:]
    print("Samples with lowest visible amount:")
    for i in range(len(lowest)-1,-1,-1):
        w = lowest[i]
        print( "\t{}: {:.2f}% (mean err: {:.2e})".format(w["index"], 100*w["visibleAmount"], w["meanDisplacementErr"]) )

    # Find samples with largest displacement to visible amount ratio:
    for i in range(len(stats)):
        stats[i]["ratio"] = stats[i]["maxTargetDisplacement"]/stats[i]["visibleAmount"]
    sortedStats = sorted(stats, key=lambda i: i["ratio"])
    worstRatios = sortedStats[-10:]
    print("Samples with largest max target displacement to visible surface amount ratio")
    for i in range(len(worstRatios)-1,-1,-1):
        w = worstRatios[i]
        print( "\t{}: {:.2e} ({:.2e}/{:.2f})".format(w["index"], w["ratio"], w["maxTargetDisplacement"], w["visibleAmount"] ) )



    # Find samples with largest ratio of error to mean target displacemnt:
    for i in range(len(stats)):
        if stats[i]["meanTargetDisplacement"] > 0.01:
            stats[i]["ratio"] = stats[i]["meanDisplacementErr"]/stats[i]["meanTargetDisplacement"]
        else:
            stats[i]["ratio"] = -1
    sortedStats = sorted(stats, key=lambda i: i["ratio"])
    worstRatios = sortedStats[-10:]
    print("Samples with worst mean error to mean target displacement ratios:")
    for i in range(len(worstRatios)-1,-1,-1):
        w = worstRatios[i]
        print( "\t{}: {:.2e} ({:.2e}/{:.2e})".format(w["index"], w["ratio"], w["meanDisplacementErr"], w["meanTargetDisplacement"] ) )

    # Find samples with largest ratio of error to max target displacemnt:
    for i in range(len(stats)):
        if stats[i]["maxTargetDisplacement"] > 0.01:
            stats[i]["ratio"] = stats[i]["meanDisplacementErr"]/stats[i]["maxTargetDisplacement"]
        else:
            stats[i]["ratio"] = -1
    sortedStats = sorted(stats, key=lambda i: i["ratio"])
    worstRatios = sortedStats[-10:]
    print("Samples with worst mean error to max target displacement ratios:")
    for i in range(len(worstRatios)-1,-1,-1):
        w = worstRatios[i]
        print( "\t{}: {:.2e} ({:.2e}/{:.2e})".format(w["index"], w["ratio"], w["meanDisplacementErr"], w["maxTargetDisplacement"] ) )


    # Find samples with largest ratio of max error to mean error:
    for i in range(len(stats)):
        if stats[i]["meanDisplacementErr"] > 0.002:
            if stats[i]["maxDisplacementErr"] > 0.01:
                stats[i]["ratio"] = stats[i]["maxDisplacementErr"]/stats[i]["meanDisplacementErr"]
            else:
                stats[i]["ratio"] = -1
        else:
            stats[i]["ratio"] = -1
    sortedStats = sorted(stats, key=lambda i: i["ratio"])
    worstRatios = sortedStats[-10:]
    print("Samples with largest max error to mean error ratios:")
    for i in range(len(worstRatios)-1,-1,-1):
        w = worstRatios[i]
        print( "\t{}: {:.2e} ({:.2e}/{:.2e})".format(w["index"], w["ratio"], w["maxDisplacementErr"], w["meanDisplacementErr"] ) )

    # Find samples with low mean error and low vis. surface but high mean target displacement:
    for i in range(len(stats)):
        if stats[i]["meanTargetDisplacement"] > 0.005:
            stats[i]["ratio"] = stats[i]["meanDisplacementErr"]*stats[i]["visibleAmount"]/stats[i]["meanTargetDisplacement"]
        else:
            stats[i]["ratio"] = 1
    sortedStats = sorted(stats, key=lambda i: -i["ratio"])
    worstRatios = sortedStats[-10:]
    print("Samples with lowest mean error to mean target displacement")
    for i in range(len(worstRatios)-1,-1,-1):
        w = worstRatios[i]
        print( "\t{}: {:.2e} ({:.2e}/{:.2e}, {:.2f})".format(w["index"], w["ratio"], w["meanDisplacementErr"], w["meanTargetDisplacement"], w["visibleAmount"]) )

    return ret

def calcMetrics(stats):
    metrics = {}
    metrics["meanDisplacementErrorRatio"] = []
    metrics["maxDisplacementErrorRatio"] = []
    metrics["meanDisplacementOverallSamples"] = []

    print("stats len:", len(stats))
    
    for idx_i, s in enumerate(stats): # hintsnum 1 - 4
        if idx_i == 0:
            err_sum0 = 0
            for idx_j0, sample0 in enumerate(s):
                err_sum0 += sample0["meanDisplacementErr"]
            metrics["meanDisplacementOverallSamples"].append( err_sum0 / len(s))
            continue
        meanDisplacementErrorRatio = { "hintsNum": idx_i }
        meanDisplacementErrorRatio["total"] = []
        meanDisplacementErrorRatio["average"] = 0

        maxDisplacementErrorRatio = { "hintsNum": idx_i }
        maxDisplacementErrorRatio["total"] = []
        maxDisplacementErrorRatio["average"] = 0

        err_sum = 0
        
        for idx_j, sample in enumerate(s):
            index_real = sample["index"]
            sample_base = None
            for sample_base_iter in stats[0]:
                if sample_base_iter["index"] == index_real:
                    sample_base = sample_base_iter
                    break
            # mean_ratio = sample["meanDisplacementErr"] / stats[0][idx_j]["meanDisplacementErr"]
            # # print(sample["meanDisplacementErr"], stats[0][idx_j]["meanDisplacementErr"],  mean_ratio)
            # meanDisplacementErrorRatio["total"].append({"index":sample["index"], "value":mean_ratio})

            # max_ratio = sample["maxDisplacementErr"] / stats[0][idx_j]["maxDisplacementErr"]
            # maxDisplacementErrorRatio["total"].append({"index":sample["index"], "value":max_ratio})

            mean_ratio = sample["meanDisplacementErr"] / sample_base["meanDisplacementErr"]
            # print(sample["meanDisplacementErr"], stats[0][idx_j]["meanDisplacementErr"],  mean_ratio)
            meanDisplacementErrorRatio["total"].append({"index":sample["index"], "value":mean_ratio})

            max_ratio = sample["maxDisplacementErr"] / sample_base["maxDisplacementErr"]
            maxDisplacementErrorRatio["total"].append({"index":sample["index"], "value":max_ratio})

            err_sum += sample["meanDisplacementErr"]

        meanDisplacementErrorRatio["average"] = sum([a["value"] for a in meanDisplacementErrorRatio["total"]]) / len(meanDisplacementErrorRatio["total"])
        maxDisplacementErrorRatio["average"] = sum([a["value"] for a in maxDisplacementErrorRatio["total"]]) / len(maxDisplacementErrorRatio["total"])

        metrics["meanDisplacementErrorRatio"].append(meanDisplacementErrorRatio)
        metrics["maxDisplacementErrorRatio"].append(maxDisplacementErrorRatio)
        metrics["meanDisplacementOverallSamples"].append( err_sum / len(s))
    return metrics


#plt.savefig( os.path.join( args.outputFolder, "ErrsOverVisAmounts.png"), bbox_inches='tight')


def calcStats_hintsbased(stats, metrics):
    summary_rank = {}
    summary_rank["meanDisplacementErr_10worst"] = []
    summary_rank["meanDisplacementErrRatio_10worst"] = []
    summary_rank["meanDisplacementErrRatio_10best"] = []
    
    # Find samples with largest error in displacement:
    # meanDisplacementErr_ranklist = []
    for idx_i, s in enumerate(stats):
        ranklist = {
            "hintsNum": idx_i-1,
            "rank"    : [],
        }
        # print("hintsnum:", idx_i)
        sortedStats = sorted(s, key=lambda i: i["meanDisplacementErr"])
        worst = sortedStats[-10:]
        # print("Samples with worst mean error:")
        for i in range(len(worst)-1,-1,-1):
            w = worst[i]
            # print( "\t{}: {}".format(w["index"], w["meanDisplacementErr"] ) )
            ranklist["rank"].append({
                "index"         :           w["index"],
                "meanDisplacementErr":      w["meanDisplacementErr"],
            })
        summary_rank["meanDisplacementErr_10worst"].append(ranklist)
        

    # meanDisplacementErrRatio_ranklist = []
    for idx_i, s in enumerate(metrics["meanDisplacementErrorRatio"]):
        ranklist = {
            "hintsNum": idx_i-1,
            "rank"    : [],
        }
        # print("hintsnum:", idx_i)
        sortedStats = sorted(s["total"], key=lambda i: i["value"])
        worst = sortedStats[-10:]
        # print("Samples with worst mean error:")
        for i in range(len(worst)-1,-1,-1):
            w = worst[i]
            # print( "\t{}: {}".format(w["index"], w["meanDisplacementErr"] ) )
            ranklist["rank"].append({
                "index"         :           w["index"],
                "meanDisplacementErrRatio":      w["value"],
            })
        summary_rank["meanDisplacementErrRatio_10worst"].append(ranklist)


    for idx_i, s in enumerate(metrics["meanDisplacementErrorRatio"]):
        ranklist = {
            "hintsNum": idx_i-1,
            "rank"    : [],
        }
        # print("hintsnum:", idx_i)
        sortedStats = sorted(s["total"], key=lambda i: i["value"])
        best = sortedStats[:10]
        # print("Samples with worst mean error:")
        for i in range(len(best)-1,-1,-1):
            w = best[i]
            # print( "\t{}: {}".format(w["index"], w["meanDisplacementErr"] ) )
            ranklist["rank"].append({
                "index"         :           w["index"],
                "meanDisplacementErrRatio":      w["value"],
            })
        summary_rank["meanDisplacementErrRatio_10best"].append(ranklist)


    return summary_rank


def statsOverHintsNum(stats):
    samples_amount = len(stats[0])
    new_stats = [{}] * samples_amount

    for idx_s in range(samples_amount):
        new_stats[idx_s]["index"] = stats[0][idx_s]["index"]
        new_stats[idx_s]["stats"] = []
        for idx_h in range(len(stats)):
            new_stats[idx_s]["stats"].append(stats[idx_h][idx_s])
            

        
        
        
    
def find_error_decreasing_samples(stats):
    samples_list = {
        "meanDisplacementErrOverhints":[],
        "maxDisplacementErrOverhints":[],
    }
    hints_num = len(stats)
    samples_num = len(stats[0])
    for idx_s in range(samples_num):
        meanDisplacementErr_overhints = []
        maxDisplacementErr_overhints = []
        for idx_h in range(hints_num):
            meanDisplacementErr_overhints.append(stats[idx_h][idx_s]["meanDisplacementErr"])
            maxDisplacementErr_overhints.append(stats[idx_h][idx_s]["maxDisplacementErr"])
        is_descending_mean = all(meanDisplacementErr_overhints[i] >= meanDisplacementErr_overhints[i+1] for i in range(len(meanDisplacementErr_overhints)-1))
        is_descending_max = all(maxDisplacementErr_overhints[i] >= maxDisplacementErr_overhints[i+1] for i in range(len(maxDisplacementErr_overhints)-1))
        if is_descending_mean:
            samples_list["meanDisplacementErrOverhints"].append({
                "index": stats[0][idx_s]["index"],
                "meanDisplacementErrOverhints": meanDisplacementErr_overhints,
            })
        if is_descending_max:
            samples_list["maxDisplacementErrOverhints"].append({
                "index": stats[0][idx_s]["index"],
                "maxDisplacementErrOverhints": maxDisplacementErr_overhints,
            })
    
    return samples_list


def generatePlots( stats, outputFolder ):
    losses = [d["loss"] for d in stats]
    visAmounts = [d["visibleAmount"]*100 for d in stats]
    meanDisplacementErrs = [d["meanDisplacementErr"]*1000 for d in stats]
    maxDisplacementErrs = [d["maxDisplacementErr"]*1000 for d in stats]
    meanTargetDisplacements = [d["meanTargetDisplacement"]*1000 for d in stats]
    maxTargetDisplacements = [d["maxTargetDisplacement"]*1000 for d in stats]
    meanDisplacements = [d["meanDisplacement"]*1000 for d in stats]
    maxDisplacements = [d["maxDisplacement"]*1000 for d in stats]
    markerScale = [(s/10+2)**2 for s in visAmounts]
    #cmap = plt.cm.get_cmap("autumn")
    cmap = plt.cm.get_cmap("plasma")

    bins = []
    binContent = []
    numBins = 10
    binSize = 1/(numBins)
    for i in range(0,numBins):
        v = binSize*(i+0.5)
        bins.append(v*100)
        binContent.append( [] )

    for d in stats:
        i = int(d["visibleAmount"]*(numBins))
        if i > numBins-1:
            i = numBins - 1
        binContent[i].append( d["meanDisplacementErr"]*1000 )

    avgBinContent = []
    nonEmptyBins = []
    for i in range(0,numBins):
        if len(binContent[i]) > 0:
            nonEmptyBins.append(bins[i])
            avgBinContent.append( sum(binContent[i])/len(binContent[i]) )

    import matplotlib.lines as mlines
    s = numpy.sqrt(min(markerScale))+3
    minValLegend = mlines.Line2D([], [], color="w", markerfacecolor=cmap(0.), marker='o',
            markersize=s, label=round(min(visAmounts),1))

    meanMarkerSize = (max(markerScale)-min(markerScale))/2
    meanVisAmount = (max(visAmounts)-min(visAmounts))/2
    s = numpy.sqrt(meanMarkerSize)*(32/25)
    meanValLegend = mlines.Line2D([], [], color="w", markerfacecolor=cmap(0.5), marker='o',
            markersize=s, label=round(meanVisAmount,1))

    s = numpy.sqrt(max(markerScale))*(32/25)
    maxValLegend = mlines.Line2D([], [], color="w", markerfacecolor=cmap(1.), marker='o',
            markersize=s, label=round(max(visAmounts),1))


    plt.clf()
    plt.figure(figsize=(8,3))
    plt.plot(nonEmptyBins,avgBinContent, "*-" )
    plt.xlabel( "Visible amount (%)" )
    plt.ylabel( "Mean displacement error (mm)" )
    plt.savefig( os.path.join( outputFolder, "MeanErrsOverVisAmounts.eps"), bbox_inches='tight')

    plt.clf()
    plt.figure(figsize=(8,8))
    plt.scatter( visAmounts, meanDisplacementErrs, s=markerScale, c=visAmounts, cmap=cmap )
    plt.xlabel( "Visible amount (%)" )
    plt.ylabel( "Mean displacement error (mm)" )
    plt.axis("equal")
    plt.grid(True)
    plt.legend(handles=[maxValLegend, meanValLegend, minValLegend], title="Vis. Surface (%)")
    plt.xlim( 0, 100 )
    plt.xticks( [i*10 for i in range(10)] )
    plt.savefig( os.path.join( outputFolder, "MeanErrsOverVisAmountsScatter.eps"), bbox_inches='tight')

    plt.clf()
    plt.figure(figsize=(8,2.5))
    plt.scatter(meanTargetDisplacements, meanDisplacementErrs, s=markerScale, c=visAmounts, cmap=cmap )
    plt.xlabel( "Mean target displacement (mm)" )
    plt.ylabel( "Mean displ. error (mm)" )
    #plt.gca().set_aspect("equal")
    plt.grid(True)
    plt.legend(handles=[maxValLegend, meanValLegend, minValLegend], title="Vis. Surface (%)")
    plt.xlim( 0, 70 )
    plt.ylim( 0, 60 )
    #plt.xticks( [i*10 for i in range(10)] )
    plt.yticks( [i*10 for i in range(7)] )
    plt.savefig( os.path.join( outputFolder, "MeanErrsOverMeanTargetDisplacement.eps"), bbox_inches='tight')

    plt.clf()
    plt.scatter(maxTargetDisplacements, maxDisplacementErrs, s=markerScale, c=visAmounts, cmap=cmap )
    plt.xlabel( "Max target displacement (mm)" )
    plt.ylabel( "Max displacement error (mm)" )
    plt.gca().set_aspect("equal")
    plt.grid(True)
    plt.legend(handles=[maxValLegend, meanValLegend, minValLegend], title="Vis. Surface (%)")
    plt.xticks( [i*10 for i in range(10)] )
    plt.yticks( [i*10 for i in range(8)] )
    plt.savefig( os.path.join( outputFolder, "MaxErrsOverMaxTargetDisplacement.eps"), bbox_inches='tight')


    plt.clf()
    #fig = plt.figure(figsize=(7,6))

    plt.scatter( meanTargetDisplacements, meanDisplacements, s=markerScale, c=visAmounts, cmap=cmap )
    plt.xlabel( "Mean target displacement (mm)" )
    plt.ylabel( "Mean displacement (mm)" )
    #plt.gca().set_adjustable("box")
    #plt.gca().axis("square")
    plt.gca().set_aspect("equal")
    plt.grid(True)
    plt.legend(handles=[maxValLegend, meanValLegend, minValLegend], title="Vis. Surface (%)")
    plt.xticks( [i*10 for i in range(10)] )
    plt.yticks( [i*10 for i in range(10)] )
    plt.savefig( os.path.join( outputFolder, "MeanDisplacements.eps"), bbox_inches='tight')
    #print( max(meanTargetDisplacements), max(meanDisplacements))

    plt.clf()
    scat = plt.scatter( maxTargetDisplacements, maxDisplacements, s=markerScale, c=visAmounts, cmap=cmap )
    plt.xlabel( "Max target displacement (mm)" )
    plt.ylabel( "Max displacement (mm)" )
    plt.xlim( 0, 100 )
    plt.ylim( 0, 100 )
    plt.xticks( [i*10 for i in range(10)] )
    plt.yticks( [i*10 for i in range(10)] )
    plt.legend(handles=[maxValLegend, meanValLegend, minValLegend], title="Vis. Surface (%)")
    plt.axis("equal")
    plt.grid(True)
    plt.legend()
    #handles, labels = scat.legend_elements(prop="sizes", alpha=0.6)
    #legend2 = plt.gca().legend(handles, labels, loc="upper right", title="Sizes")
    plt.savefig( os.path.join( outputFolder, "MaxDisplacements.eps"), bbox_inches='tight')


def generateMetricsPlots(metrics, outputFolder):
    
    # print(len(metrics["meanDisplacementErrorRatio"]["total"]))
    average_ratio_list = [1.0]
    for idx, m in enumerate(metrics["meanDisplacementErrorRatio"]):
        plt.clf()
        average_ratio_list.append(m["average"])

        meanDisplacementErrFactor = [a["value"] for a in m["total"]]
        print(len(meanDisplacementErrFactor))
        meanDisplacementErrFactor_hist = plt.hist(meanDisplacementErrFactor, bins=15, facecolor="blue", edgecolor="black", alpha=0.7)
        plt.xlabel("range")
        plt.ylabel("frequence")
        plt.title("DisplacementErrFactor hist")
        
        plt.savefig( os.path.join( outputFolder, "DisplacementErrFactor_hinsnum{}.eps".format(idx)))
        plt.show()

    x = range(0, len(average_ratio_list))
    
    meanDisplacementErrFactor_average_plot = plt.plot(x, average_ratio_list, "r", marker='*', ms=10, label="a")
    plt.xlabel("hitns number")
    plt.ylabel("mean displacement error")
    plt.title("mean displacement error over hints numbers")
    plt.savefig( os.path.join( outputFolder, "meanDisplacementErrFactor.eps"))
    plt.show()

if __name__ == "__main__":
    import argparse
    import pickle

    parser = argparse.ArgumentParser(description="Train model on deformation data.")
    parser.add_argument("folder", type=path, help="Folder in which to save the plots. Must contain a 'results.pkl' as generated by apply.py")
    args = parser.parse_args()


    # Load results previously calculated by apply.py:
    # filename = os.path.join( args.folder, "results.pkl" )
    # if not os.path.exists( filename ):
    #     raise IOError("Could not find 'results.pkl' in {}!".format( args.folder ))
    
    # stats = pickle.load( open( filename, 'rb' ) )

    # calcStats( stats )
    # generatePlots( stats, args.folder )

    # filename = os.path.join( args.folder, "metrics.pkl" )
    # if not os.path.exists( filename ):
    #     raise IOError("Could not find 'metrics.pkl' in {}!".format( args.folder ))
    
    # metrics = pickle.load( open( filename, 'rb' ) )
    # print(len(metrics["meanDisplacementErrorRatio"]))
    # print("meanDisplacementErrorRatio:")
    # for m in metrics["meanDisplacementErrorRatio"]:
    #     print(m["average"])
    # print("maxDisplacementErrorRatio:")
    # for m in metrics["maxDisplacementErrorRatio"]:
    #     print(m["average"])


    # stats0 = pickle.load( open( "/home/liupeng/1_Code/tests/trained_28_03_origin_5k/results.pkl", 'rb' ) )
    # stats0 = pickle.load( open( "/home/liupeng/1_Code/tests/trained_09_04_origin_wihouthints/results.pkl", 'rb' ) )
    # stats0 = pickle.load( open( "/home/liupeng/1_Code/tests/trained_15_04_baseline_origin_5k/results.pkl", 'rb' ) )
    # stats0 = pickle.load( open( "/home/liupeng/1_Code/tests/trained_15_04_baseline_origin_5k_500/results.pkl", 'rb' ) )
    stats0 = pickle.load( open("/mnt/ceph/tco/TCO-Staff/Homes/liupeng/V2SNet/tests/trained_80k_origin_baseline_500_shifted_idx_5000_5500/results.pkl", "rb"))
    print(len(stats0))
    folder_name = str(args.folder).split("/")[-1]
    folder_list = os.listdir(args.folder)
    folder_list.sort()
    # print(folder_list)
    # sys.exit(0)
    stats = [stats0]
    # print(stats)
    # sys.exit(0)
    for p in folder_list:
        if os.path.isdir(os.path.join(args.folder, p)):
                # stats1 = pickle.load( open( os.path.join(args.folder, folder_name + "_1/results.pkl"), 'rb' ) )
            print(p)
            stat = pickle.load( open( os.path.join(args.folder, p + "/results.pkl"), 'rb' ) )
            calcStats(stat)
            stats.append(stat)
        
        # stats2 = pickle.load( open( os.path.join(args.folder, folder_name + "_2/results.pkl"), 'rb' ) )
        # stats3 = pickle.load( open( os.path.join(args.folder, folder_name + "_3/results.pkl"), 'rb' ) )
        # stats4 = pickle.load( open( os.path.join(args.folder, folder_name + "_4/results.pkl"), 'rb' ) )

    # sys.exit(0)

    print(len(stats))

    # sys.exit(0)
    # stats = [stats0, stats1, stats2, stats3, stats4]
    metrics_all = calcMetrics(stats)
    print("meanDisplacementErrorRatio:")
    for m in metrics_all["meanDisplacementErrorRatio"]:
        print(m["average"])
    print("maxDisplacementErrorRatio:")
    for m in metrics_all["maxDisplacementErrorRatio"]:
        print(m["average"])
    
        # print(metrics_all)
    print(metrics_all["meanDisplacementOverallSamples"])

    # sys.exit(0)
    # with open(os.path.join("/home/liupeng/1_Code/tests/trained_22_03_autoencoder_DF_weightX10_5k", "metrics_30_03_basedon_origin.pkl"), 'wb') as pkl_file:
    with open(os.path.join(args.folder, "metrics.pkl"), 'wb') as pkl_file:
        # print("Writing results to:", os.path.join(outputFolder_base, "metrics.pkl"))
        pickle.dump( metrics_all, pkl_file )
        pkl_file.close()
    
    # sys.exit(0)
    # generateMetricsPlots(metrics_all, args.folder)


    rank = calcStats_hintsbased(stats, metrics_all)
    print("meanDisplacementErr 10 worst")
    for i in rank["meanDisplacementErr_10worst"]:
        if i["hintsNum"] == -1:
            print("baseline")
        else:
            print("hintsNum", i["hintsNum"])
        for j in i["rank"]:
            print(j["index"], j["meanDisplacementErr"])

    print("\nmeanDisplacementErrRatio 10 worst")
    for i in rank["meanDisplacementErrRatio_10worst"]:
        print("hintsNum", i["hintsNum"] + 1)
        for j in i["rank"]:
            print(j["index"], j["meanDisplacementErrRatio"])

    print("\nmeanDisplacementErrRatio 10 best")
    for i in rank["meanDisplacementErrRatio_10best"]:
        print("hintsNum", i["hintsNum"] + 1)
        for j in i["rank"]:
            print(j["index"], j["meanDisplacementErrRatio"])
    
    metrics_all["rank"] = rank
    
    with open(os.path.join(args.folder, "metrics_rank.pkl"), 'wb') as pkl_file:
        print("Writing results to:", os.path.join(args.folder, "metrics_rank.pkl"))
        pickle.dump( metrics_all, pkl_file )
        pkl_file.close()
                
    # descending_list = find_error_decreasing_samples(stats)
    # print(len(descending_list))
    # print(descending_list["meanDisplacementErrOverhints"])
    # print([index["index"] for index in descending_list["meanDisplacementErrOverhints"]])
    # print(descending_list["maxDisplacementErrOverhints"])
    # print([index["index"] for index in descending_list["maxDisplacementErrOverhints"]])
