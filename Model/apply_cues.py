import sys, os
import traceback
sys.path.append("../")
from DataGeneration import vtkutils
from DataGeneration import voxelize
import torch
from vtk.util import numpy_support
import vtk
import numpy




def voxelize_user_input(voxel_grid, user_input_folder, transform=True, cues_num=4,):
    """Voxelize the matching cues marked by users based on the output of V2S-Net.

    Keyword arguments:
    voxel_grid -- the target voxel grid
    user_input_folder -- folder where the matching cues lie
    transform -- whether to transform the matching cues
    cues_num -- number of matching cues marked by the users
    """
    try:
        # voxel_grid = vtkutils.loadStructuredGrid(os.path.join(voxel_grid_folder, "voxelizedResult.vts"))
        voxel_grid = voxel_grid

        if not os.path.exists(os.path.join(user_input_folder, "vtk_format")):
            os.makedirs(os.path.join(user_input_folder, "vtk_format"))

        if transform:
            tf = vtkutils.loadTransformationMatrix(voxel_grid)

        user_input_hints_list_pre = []
        user_input_hints_list_intra = []
        for idx_h in range(0, 4):
            if idx_h < cues_num:
                pre_hint_path = list(filter(lambda x:x.endswith("output_src_region{}.pcd".format(idx_h)), os.listdir(user_input_folder)))[0]
                print(pre_hint_path)
                pre_hint = vtkutils.loadMesh(os.path.join(user_input_folder, pre_hint_path))
                vtkutils.writeMesh(pre_hint, os.path.join(user_input_folder, "vtk_format", pre_hint_path[:-4] + ".vtp"))
                print("save point cloud of pre cue as .vtp to:", os.path.join(user_input_folder, "vtk_format", pre_hint_path[:-4] + ".vtp"))
                if transform:
                    tfFilter = vtk.vtkTransformFilter()
                    tfFilter.SetTransform(tf)
                    tfFilter.SetInputData(pre_hint)
                    tfFilter.Update()
                    pre_hint = tfFilter.GetOutput()
                    user_input_hints_list_pre.append(pre_hint)
                voxelize.distanceFieldGPU(surface=pre_hint, targetGrid=voxel_grid, targetArrayName="hints_index_pre_DF_{}".format(idx_h))

                intra_hint_path = list(filter(lambda x:x.endswith("output_target_region{}.pcd".format(idx_h)), os.listdir(user_input_folder)))[0]
                print(intra_hint_path)
                intra_hint = vtkutils.loadMesh(os.path.join(user_input_folder, intra_hint_path))
                vtkutils.writeMesh(intra_hint, os.path.join(user_input_folder, "vtk_format", intra_hint_path[:-4] + ".vtp"))
                print("save point cloud of intra cue as .vtp to:", os.path.join(user_input_folder, "vtk_format", intra_hint_path[:-4] + ".vtp"))
                if transform:
                    tfFilter = vtk.vtkTransformFilter()
                    tfFilter.SetTransform(tf)
                    tfFilter.SetInputData(intra_hint)
                    tfFilter.Update()
                    intra_hint = tfFilter.GetOutput()
                    user_input_hints_list_intra.append(intra_hint)
                voxelize.distanceFieldGPU(surface=intra_hint, targetGrid=voxel_grid, targetArrayName="hints_index_intra_DF_{}".format(idx_h))
            else:
                print("add blank arrays if no cues are marked")
                vtkutils.add_blank_array(mesh=voxel_grid, array_name="hints_index_pre_DF_{}".format(idx_h))
                vtkutils.add_blank_array(mesh=voxel_grid, array_name="hints_index_intra_DF_{}".format(idx_h))

        return voxel_grid

    except Exception as e:
        traceback.print_exc()



def deform_pre_surface(pre_surface, voxel_grid, output_folder=None):
    """Deformed the preoperative surface based on the predicted displacement field of V2S-Net.

    Keyword arguments:
    pre_surface -- preoperative surface
    voxel_grid -- voxel grid where the estimated displacement field lies
    output_folder -- folders where to save the data
    """
    print("deforming deformed preoperative surface by V2S-Net...")
    output_filename_withDispl = "pre_surface_with_displ.vtu"
    output_filename_deformed = "pre_surface_deformed.vtu"
    pre_surface_deformed = warp_arbitrary_mesh(
        mesh=pre_surface,
        voxel_grid=voxel_grid,
        output_folder=output_folder,
        output_filename_deformed=output_filename_deformed,
        output_filename_withDispl=output_filename_withDispl,
    )

    tf = vtkutils.loadTransformationMatrix(voxel_grid)
    tfFilter = vtk.vtkTransformFilter()
    tfFilter.SetTransform(tf)
    tfFilter.SetInputData(pre_surface_deformed)
    tfFilter.Update()
    pre_surface_deformed = tfFilter.GetOutput()
    voxelize.distanceFieldGPU(surface=pre_surface_deformed, targetGrid=voxel_grid, targetArrayName="preoperativeSurface")
    voxelize.to_SDF(surfaceMesh=vtkutils.unstructuredGridToPolyData(pre_surface_deformed), targetGrid=voxel_grid, targetArrayName="preoperativeSurface")
    
    tf.Inverse()
    tfFilter = vtk.vtkTransformFilter()
    tfFilter.SetTransform( tf )
    tfFilter.SetInputData( pre_surface_deformed )
    tfFilter.Update()
    pre_surface_deformed = tfFilter.GetOutput()
    return pre_surface_deformed



def deform_preoperative_cues(voxel_grid, user_input_folder, cues_num):
    """Deformed the preoperative matching cues based on the predicted displacement field of V2S-Net.

    Keyword arguments:
    voxel_grid -- voxel grid where the estimated displacement field lies
    user_input_folder -- folders where matching cues lie
    cues_num -- the number of matching cues marked by users
    """
    print("warp preoperative cues...")
    # pre_cues_list = []
    output_folder = os.path.join(user_input_folder, "vtk_format/pre_cue_deformed_cue-net")
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    for idx_h in range(0, cues_num):
        pre_cue_filename = list(filter(lambda x:x.endswith("output_src_region{}.pcd".format(idx_h)), os.listdir(user_input_folder)))[0]
        pre_cue_path = os.path.join(user_input_folder, pre_cue_filename)
        # pre_cues_list.append(vtkutils.loadMesh(pre_cue_path))
        pre_cue = vtkutils.loadMesh(pre_cue_path)

        warp_arbitrary_mesh(
            mesh=pre_cue, 
            voxel_grid=voxel_grid, 
            output_folder=output_folder, 
            output_filename_deformed=pre_cue_filename.replace( ".pcd" ,"_deformed_cue-net.vtu"),
        )



def get_correpsondance_for_intra_surface(pre_surface, intra_surface, voxel_grid, output_folder=None):
    """Calculate the correspondece surface patch of intraoperative surface on the deformed preoperative surface by V2S-Net .

    Keyword arguments:
    pre_surface -- preoperative surface deformed by V2S-Net
    intra_surface -- target intraoperative surface
    voxel_grid -- voxel grid where to save the surface patch
    output_folder -- folder where to save the surface patch in .vtp format
    """
    try:
        pre_surface_deformed = pre_surface
        intra_surface = intra_surface
        voxel_grid = voxel_grid

        correspondance_on_deformed_pre_array = vtk.vtkDoubleArray()
        correspondance_on_deformed_pre_array.SetNumberOfComponents(1)
        correspondance_on_deformed_pre_array.SetNumberOfTuples(pre_surface_deformed.GetNumberOfPoints())
        correspondance_on_deformed_pre_array.SetName("correspondance_on_deformed_pre_array")
        if pre_surface_deformed.GetPointData().HasArray("correspondance_on_deformed_pre_array"):
            pre_surface_deformed.GetPointData().RemoveArray("correspondance_on_deformed_pre_array")
        for idx in range(pre_surface_deformed.GetNumberOfPoints()):
            correspondance_on_deformed_pre_array.SetTuple(idx, [0,]) 
        pre_surface_deformed.GetPointData().AddArray(correspondance_on_deformed_pre_array)
        correspondance_on_deformed_pre_array = pre_surface_deformed.GetPointData().GetArray("correspondance_on_deformed_pre_array")

        locator = vtk.vtkPointLocator()
        locator.SetDataSet(pre_surface_deformed)
        locator.Update()

        for i in range(0, intra_surface.GetNumberOfPoints()):
            radius = 0.008
            cell_pts_idx = vtk.vtkIdList()
            cell_pts_valid = False
            while not cell_pts_valid:
                locator.FindPointsWithinRadius( radius, intra_surface.GetPoint(i), cell_pts_idx)
                # print("number of nearest points:", cell_pts_idx.GetNumberOfIds())
                if cell_pts_idx.GetNumberOfIds() == 0:
                    # print(i, "number of nearest points:", cell_pts_idx.GetNumberOfIds())
                    radius += 0.001
                    # print("could not find the closet point(s) for point {}, increase radius to {} and re-generate...".format(i, radius))
                else:
                    cell_pts_valid = True

            for j in range(cell_pts_idx.GetNumberOfIds()):
                p_id = cell_pts_idx.GetId(j)
                correspondance_on_deformed_pre_array.SetTuple(p_id, [1,]) 

        thresh = vtk.vtkThresholdPoints()
        thresh.SetInputData( pre_surface_deformed )
        thresh.SetInputArrayToProcess(0, 0, vtk.vtkDataObject.FIELD_ASSOCIATION_POINTS, vtk.vtkDataSetAttributes.SCALARS, "correspondance_on_deformed_pre_array" )
        thresh.ThresholdByUpper(0.9)
        thresh.Update()

        correspondance_on_deformed_pre = thresh.GetOutput()
        print(correspondance_on_deformed_pre.GetNumberOfPoints())
        correspondance_on_deformed_pre = vtkutils.unstructuredGridToPolyData( thresh.GetOutput() )

        if output_folder:
            vtkutils.writeMesh(correspondance_on_deformed_pre, os.path.join(output_folder, "correspondance_on_deformed_pre.vtp"))
            print("write mesh to", os.path.join(output_folder, "correspondance_on_deformed_pre.vtp"))

        tf = vtkutils.loadTransformationMatrix(voxel_grid)
        tfFilter = vtk.vtkTransformFilter()
        tfFilter.SetTransform(tf)
        tfFilter.SetInputData(correspondance_on_deformed_pre)
        tfFilter.Update()
        correspondance_on_deformed_pre = tfFilter.GetOutput()
        voxelize.distanceFieldGPU(surface=correspondance_on_deformed_pre, targetGrid=voxel_grid, targetArrayName="correspondance_of_intra_on_deformed_pre")

        return voxel_grid
    except Exception as e:
        traceback.print_exc()



def load_model(model_folder):
    """Load trained Cue-Net .

    Keyword arguments:
    model folder -- folder where the trained Cue-Net lies
    """
    sys.path.append(model_folder)

    import model
    import checkpoint

    nn = model.Model().cuda()

    checkpoint.load( os.path.join(model_folder, "checkpoints"), nn )
    print("loaded model from", model_folder)
    return nn



def saveSample( voxel_grid, arraysToAppend, outPath="" ):

    voxels = voxel_grid
    for name, tensor in arraysToAppend.items():
        if type(tensor) == torch.Tensor:
            np = tensor.detach().cpu().numpy()
            np = numpy.transpose( np, (1,2,3,0) )
            gridSize = np.shape[0]
            components = np.shape[3]
            np = numpy.reshape( np, (gridSize**3, components) )
            arr = numpy_support.numpy_to_vtk( np )
            arr.SetName( name )
            if voxels.GetPointData().HasArray(name):
                voxels.GetPointData().RemoveArray(name)
                print("Warning: Overwriting array {} (original file will stay untouched)".format(name))
            voxels.GetPointData().AddArray(arr)

    if outPath:
        folder = os.path.dirname( outPath )
        if not os.path.exists( folder ):
            os.makedirs( folder )
        
        vtkutils.writeMesh(voxels, filename=outPath)
        # writer = vtk.vtkXMLStructuredGridWriter()
        # writer.SetFileName( outPath )
        # writer.SetInputData( voxels )
        # writer.Update()

    return voxels


def register(nn, voxel_grid):
    """Main function of using matching cues for reference with Cue-Net .

    Keyword arguments:
    nn -- trained Cue-Net
    voxel_grid -- voxel grid where all the input data lies
    """
    try:
        data = voxel_grid.GetPointData()
        preoperativeRaw = numpy_support.vtk_to_numpy( data.GetArray("preoperativeSurface") )
        intraoperativeRaw = numpy_support.vtk_to_numpy( data.GetArray("intraoperativeSurface") )
        intraoperative_corresp_on_pre_Raw = numpy_support.vtk_to_numpy( data.GetArray("correspondance_of_intra_on_deformed_pre") )

        gridSize = 64
        intraoperative = numpy.reshape( intraoperativeRaw, (gridSize, gridSize, gridSize, 1) )
        intraoperative = torch.FloatTensor(numpy.transpose(intraoperative, (3,0,1,2) )).cuda().unsqueeze(0)
        preoperative = numpy.reshape( preoperativeRaw, (gridSize, gridSize, gridSize, 1) )
        preoperative = torch.FloatTensor(numpy.transpose(preoperative, (3,0,1,2) )).cuda().unsqueeze(0)
        intraoperative_corresp_on_pre = numpy.reshape( intraoperative_corresp_on_pre_Raw, (gridSize, gridSize, gridSize, 1) )
        intraoperative_corresp_on_pre = torch.FloatTensor(numpy.transpose(intraoperative_corresp_on_pre, (3,0,1,2) )).cuda().unsqueeze(0)
        
        # load matching cues
        hintsindexarraypre_list = []
        hintsindexarrayintra_list = []
        for hints_idx in range(0, 4):
            hintsindexarraypreRaw = numpy_support.vtk_to_numpy(data.GetArray("hints_index_pre_DF_{}".format(hints_idx)))
            hintsindexarrayintraRaw = numpy_support.vtk_to_numpy(data.GetArray("hints_index_intra_DF_{}".format(hints_idx)))
            hintsindexarraypreSingle = numpy.reshape( hintsindexarraypreRaw, (gridSize, gridSize, gridSize, 1) )
            hintsindexarraypreSingle = numpy.transpose(hintsindexarraypreSingle, (3,0,1,2) )
            hintsindexarrayintraSingle = numpy.reshape( hintsindexarrayintraRaw, (gridSize, gridSize, gridSize, 1) )
            hintsindexarrayintraSingle = numpy.transpose(hintsindexarrayintraSingle, (3,0,1,2) )
            hintsindexarraypre_list.append(hintsindexarraypreSingle)
            hintsindexarrayintra_list.append(hintsindexarrayintraSingle)

        hintsindexarraypre = torch.FloatTensor(numpy.concatenate(tuple(hintsindexarraypre_list), axis=0)).cuda().unsqueeze(0)
        hintsindexarrayintra = torch.FloatTensor(numpy.concatenate(tuple(hintsindexarrayintra_list), axis=0)).cuda().unsqueeze(0)

        # prediction with matching cues
        displ_field,_,_,_ = nn( preoperative, intraoperative, intraoperative_corresp_on_pre, hintsindexarraypre, hintsindexarrayintra )
        print("Prediction with Cues finished", displ_field.shape)
        estimatedDisplacement = (displ_field).squeeze()

        resultArrays = {}
        resultArrays["estimatedDisplacement"] = estimatedDisplacement
        # # if errDisplacement is not None:    # If a target is known, also calculate the error:
        # #     resultArrays["displacementError"] = errDisplacement

        voxel_grid = saveSample( voxel_grid, resultArrays )
        # if verbose:
        #     print("Saved sample to " + outFile)
        return voxel_grid
    except Exception as e:
        traceback.print_exc()


def warp_arbitrary_mesh( mesh, voxel_grid, output_folder="", output_filename_withDispl="", output_filename_deformed=""):
    """Deform any meshes by the give displacement field in the voxel grid.

    Keyword arguments:
    mesh -- any 3D meshes
    voxel_grid -- voxel grid where the displacement field lies
    """
    try:
        mesh = mesh
        voxel_grid = voxel_grid

        print("interpolate displacement field from voxel grid to mesh...")
        volume_with_displ = vtkutils.displ_field_interpolate(mesh, voxel_grid, output_folder, output_filename=output_filename_withDispl)
        if not volume_with_displ:
            print("interpolation went wrong!")
            return None
        print("warping......")
        volume_warped = vtkutils.warp_mesh(volume_with_displ, "estimatedDisplacement", output_folder, output_filename_deformed)
        return volume_warped
    except Exception as e:
        traceback.print_exc()
        return None


def get_mean_displacement_error(pc_folder, voxel_grid_result):
    """Calculate mean dispalcement error for the results of Cue-Net with matching cues, when ground truth information is available.

    Keyword arguments:
    pc_folder -- data folder
    voxel_grid_result -- voxel grid where the displacement field lies
    """
    try:
        # baseline_path = ""
        # with open(baseline_path, "r") as f:
        #     print("baseline results", f.read())
        #     f.close()
        # voxel_grid_result = vtkutils.loadStructuredGrid(os.path.join(displ_dir, "voxelizedResult.vts"))
        target_pc = vtkutils.loadMesh(os.path.join(pc_folder, "insp_poly.vtp"))
        source_pc = vtkutils.loadMesh(os.path.join(pc_folder, "source_pc_residual_deformed.vtu"))

        err_mags = []
        for idx_p in range(target_pc.GetNumberOfPoints()):
            point_target = numpy.asarray(target_pc.GetPoint(idx_p))
            point_source = numpy.asarray(source_pc.GetPoint(idx_p))
            err = point_source - point_target
            err_mags.append(numpy.linalg.norm(err))
        mean_target_displ = sum(err_mags)/len(err_mags)
        max_target_displ = max(err_mags)

        # source_landmark_with_displ = vtkutils.displ_field_interpolate(source_pc, voxel_grid_result, displ_dir, tf_flag=True, output_filename="source_pc_residual_withDispl.vtu")
        # source_landmark_warped = vtkutils.warp_mesh(source_landmark_with_displ, "estimatedDisplacement", displ_dir, "source_pc_residual_withDispl_deformed.vtu")
        source_landmark_warped = warp_arbitrary_mesh( source_pc, voxel_grid_result, tf_flag=True, output_filename_withDispl="", output_filename_deformed="") 
        err_mags = []
        for idx_p in range(target_pc.GetNumberOfPoints()):
            point_target = numpy.asarray(target_pc.GetPoint(idx_p))
            point_source = numpy.asarray(source_landmark_warped.GetPoint(idx_p))
            err = point_source - point_target
            err_mags.append(numpy.linalg.norm(err))
        mean_displ_err = sum(err_mags)/len(err_mags)
        max_displ_err = max(err_mags)
        # print(mean_displ_err)
        # mean_displ_err_list.append(mean_displ_err)
        
        stats = {
            'meanDisplacementErr': mean_displ_err.item(), 
            'maxDisplacementErr': max_displ_err.item(), 
            'meanTargetDisplacement': mean_target_displ.item(), 
            'maxTargetDisplacement': max_target_displ.item(),
        }

        return stats

    except Exception as e:
        traceback.print_exc()


def visualize(mesh_list):
    """Visualize the results of Cue-Net

    Keyword arguments:
    mesh_list -- list of meshes to be viusalized
    """
    print("visualization")

    colors = vtk.vtkNamedColors()
    # Set the background color.
    bkg = map(lambda x: x / 255.0, [26, 51, 102, 255])
    green_color = list(map(lambda x: x / 255.0, [173, 255, 150]))
    colors.SetColor("BkgColor", *bkg)

    # This creates a polygonal cylinder model with eight circumferential
    # facets.
    # cylinder = vtkCylinderSource()
    # cylinder.SetResolution(8)

    # The mapper is responsible for pushing the geometry into the graphics
    # library. It may also do color mapping, if scalars or other
    # attributes are defined.
    ren = vtk.vtkRenderer()
    renWin = vtk.vtkRenderWindow()
    renWin.AddRenderer(ren)
    iren = vtk.vtkRenderWindowInteractor()
    iren.SetRenderWindow(renWin)

    color_list = [
        list(map(lambda x: x / 255.0, [255, 255, 255])), # preoperative surface
        list(map(lambda x: x / 255.0, [255, 92, 74])), # deformed preoperative surface
        list(map(lambda x: x / 255.0, [173, 255, 150])), # intraoperative surface
    ]

    opacity_list = [
        0.3, 
        1.0, 
        0.7,
    ]

    for idx_m, mesh in enumerate(mesh_list):
        if not isinstance(mesh, vtk.vtkPolyData):
            mesh = vtkutils.unstructuredGridToPolyData(mesh)
        meshMapper = vtk.vtkPolyDataMapper()
        # cylinderMapper.SetInputConnection(intra.GetOutputPort())
        meshMapper.SetInputData(mesh)

        # The actor is a grouping mechanism: besides the geometry (mapper), it
        # also has a property, transformation matrix, and/or texture map.
        # Here we set its color and rotate it -22.5 degrees.
        meshActor = vtk.vtkActor()
        meshActor.SetMapper(meshMapper)
        # meshActor.GetProperty().SetColor(colors.GetColor3d("Tomato"))
        # print(colors.GetColor3d("Tomato"))
        # if idx_m == 0:
        meshActor.GetProperty().SetColor(color_list[idx_m])
        meshActor.GetProperty().SetOpacity(opacity_list[idx_m])
        # if idx_m == 0:
        #     meshActor.GetProperty().SetColor(green_color)
        #     meshActor.GetProperty().SetOpacity(0.5)
        # else:
        #     meshActor.GetProperty().SetColor(colors.GetColor3d("Tomato"))
        # meshActor.RotateX(30.0)
        meshActor.RotateX(-60.0)

        # Create the graphics structure. The renderer renders into the render
        # window. The render window interactor captures mouse events and will
        # perform appropriate camera or actor manipulation depending on the
        # nature of the events.
        # Add the actors to the renderer, set the background and size
        ren.AddActor(meshActor)

    ren.SetBackground(colors.GetColor3d("BkgColor"))
    renWin.SetSize(1000, 1000)
    renWin.SetWindowName('registration results')

    # This allows the interactor to initalize itself. It has to be
    # called before an event loop.
    iren.Initialize()

    # We'll zoom in a little by accessing the camera and invoking a "Zoom"
    # method on it.
    ren.ResetCamera()
    ren.GetActiveCamera().Zoom(1.5)

    renWin.Render()

    # Start the event loop.
    iren.Start()



if __name__ == "__main__":
    import argparse
    import pickle

    parser = argparse.ArgumentParser(description="Apply user input matching cues")
    parser.add_argument("--model_folder", type=str, default=".", help="Folder where the model lies")
    parser.add_argument("--voxel_grid_path", type=str, required=True, help="Path to voxel grid lies")
    parser.add_argument("--user_input_folder", type=str, default="", help="Folder where user inputs lie")
    parser.add_argument("--inference", action="store_true", help="Perform inference or not")
    parser.add_argument("--preprocess", action="store_true", help="Execute preprocessing or not")
    parser.add_argument("--cue_num", type=int, required=True, help="how many cues will be used")
    args = parser.parse_args()

    model_folder = args.model_folder
    voxel_grid_path = args.voxel_grid_path
    # index = args.index
    user_input_folder = args.user_input_folder
    inference = args.inference
    cue_num = args.cue_num

    # file path to the original pre and intraoperative surface
    pre_surface_path = "pre_surface.vtp"
    intra_surface_path = "intra_surface.vtp"

    voxel_grid_folder = os.path.dirname(voxel_grid_path)
    # pre_surface_deformed_path = os.path.join(voxel_grid_folder, "pre_surface_deformed.vtu")
    voxel_grid = vtkutils.loadStructuredGrid(voxel_grid_path)
    pre_surface = vtkutils.loadMesh(pre_surface_path)
    intra_surface = vtkutils.loadMesh(intra_surface_path)
    # pre_surface_deformed = vtkutils.loadMesh(pre_surface_deformed_path)
    
    # Preprocessing:
    if args.preprocess:
        # deform preoperative surface by the prediction of V2S-Net
        pre_surface_deformed = deform_pre_surface(
                    pre_surface=pre_surface,
                    voxel_grid=voxel_grid,
                    output_folder=os.path.dirname(voxel_grid_path)
        )
        # get correspondance of intra surface
        voxel_grid = get_correpsondance_for_intra_surface(
                    pre_surface=pre_surface_deformed, 
                    intra_surface=intra_surface, 
                    voxel_grid=voxel_grid,
                    output_folder=os.path.dirname(voxel_grid_path),
        )
        # voxelize matching cues and save to voxel grid
        voxel_grid = voxelize_user_input(
                    voxel_grid=voxel_grid, 
                    user_input_folder=user_input_folder, 
                    transform=True,
                    cues_num=cue_num,
        )
        vtkutils.writeMesh(voxel_grid, os.path.join(user_input_folder, "voxelized.vts"))
        print("saved voxel grid with user input to", os.path.join(user_input_folder, "voxelized.vts"))


    if inference:
        print("Apply matching cues....")
        model = load_model(model_folder=model_folder)
        voxel_grid = register(nn=model, voxel_grid=voxel_grid)
        print(voxel_grid.GetPointData().HasArray("estimatedDisplacement"))
        vtkutils.writeMesh(voxel_grid, os.path.join(voxel_grid_folder, "voxelizedResult_cuenet.vts"))
        print("saved inference result from Cue-Net with matching cues to:", os.path.join(voxel_grid_folder, "voxelizedResult_cuenet.vts"))

        # deform the results from V2S-Net by the predicted displacement field from Cue-Net with user inputs
        preoperative_surface = vtkutils.loadMesh(os.path.join(voxel_grid_folder, "pre_surface_deformed.vtu"))
        preoperative_surface_warped = warp_arbitrary_mesh(
                mesh=preoperative_surface, 
                voxel_grid=voxel_grid, 
                output_folder=user_input_folder, 
                output_filename_deformed="pre_surface_deformed_cue-net.vtu",
        )

        # deform preoperative matching cues:
        deform_preoperative_cues(
                voxel_grid=voxel_grid,
                user_input_folder=user_input_folder,
                cues_num=cue_num,
        )

    # stats = get_mean_displacement_error(pc_folder=voxel_grid_folder, voxel_grid_result=voxel_grid)
    # print(stats)
    # baseline_path = "results.txt"
    # with open(baseline_path, "r") as f:
    #     stats_baseline = ast.literal_eval(f.read())
    #     # print("baseline results", f.read())
    #     f.close()
    # print("========stats======:")
    # print("mean displacemen error with cues:", stats["meanDisplacementErr"] )
    # print("mean displacemen error of baseline:", stats_baseline[index] )
    # print("mean displacement error ratio:", stats["meanDisplacementErr"] / stats_baseline[index])


    # intraoperative_surface = vtkutils.loadMesh(os.path.join(voxel_grid_folder, "partialDeformedSurface.vtp"))
    # visualize([preoperative_surface, preoperative_surface_warped, intraoperative_surface])



