Leveraging Matching Cues for Online Adaptation in Laparoscopic Liver Registration Pipeline (Cue-Net)
================================================

In this project, we propose Cue-Net for real-time adaptation in non-rigid laparoscopic liver registration. Cue-Net takes two aligned preoperative liver volume mesh and intraoperative partial surface, and four matching cues as input. A matching cue is a pair of corresponding surface patches which should be aligned with each other. Cue-Net aims to learn the coarse corresponding information from such matching cues and improve the registration results. 


## Environments

- python=3.9
- pytorch=2.0
- tensorboardX=2.1
- vtk=9.0
- matplotlib=3.2.1
- numpy=1.22.3


## Data Generation

Scripts related to synthetic matching cues are stored under DataGenration folder. The folder structure of training dataset is shown below:

```
folder/
├── 000000
│   └── voxelized.vts
├── 000001
│   └── voxelized.vts
...
```
### Synthetic Matching Cues Extraction:
Prerequisites: inference results of all samples from V2S-Net, where estimated displacement fields are stored. Please refer to [V2S-Net](https://gitlab.com/nct_tso_public/Volume2SurfaceCNN) for instruction.

Deform preoperative liver volume and calculate the new ground truth displacement field. Command example:
```
python3 preprocess_deform.py --datapath /path/to/trainingset/  --displ_folder /path/to/v2snet/results/ --start 0 --num 500
```

Mark matching cues and voxelize them:
```
python3 extract_cues.py /path/to/trainingset/ --start 0 --num 500
```

## Train
```
cd Model/
python3 train.py /path/to/trainingset/ --outputFolder /path/to/outputfolder --num 500 

```


## Apply Matching Cues with Real Data

### Matching Cues Extraction:

Matching cues can be extracted manually using any 3D visualization tools with the function of vertices or cells selection, e.g. Paraview or Blender. The extracted matching cues should be saved under the same folder for later voxelization. Below is an example of matching cues:

![Example of matching cues](./Documentation/lld_mark_cues.png)

### Preprocessing

The 3D meshes, e.g. preoperative surface and matching cues, will be handled by the preprocessing procedure including deforming the preoperative surface by initial prediction, voxelize the deformed preoperarive surface and the matching cues. The output of preprocessing is a voxel grid where all the data required by Cue-Net is stored. Here is a command example:

```
python3 apply_cues.py --voxel_grid_path /path/to/voxelizedResult.vts --user_input_folder /path/to/extracted/matching/cues --cue_num 4 --preprocess
```

### Inference
Inference is done by inputting the voxel grid with matching cues to the trained Cue-Net:

```
python3 apply_cues.py --voxel_grid_path /path/to/voxelized.vts --cue_num 4 --preprocess --inference
```

Or together with preprocessing:

```
python3 apply_cues.py --voxel_grid_path /path/to/voxelizedResult.vts --user_input_folder /path/to/extracted/matching/cues --cue_num 4 --preprocess --inference
```

### Visualization
Here is an animation of the Cue-Net registration pipeline:

![Cue-Net registration pipeline](./Documentation/inference_with_cue-net_animation_2.gif)

